# Flowball

[![Build status](https://ci.appveyor.com/api/projects/status/pjxh5g91jpbh7t84?svg=true)](https://ci.appveyor.com/project/farerdk/flowerpot)
=================================
 
Flowball is a data visualization suite that aims to make data visualization for everyone. We aim at having a minimum of configuration and coding to be able to create standard visualizations. It shuold be possible to just select the kind of visualization you want and to point to a named data provider.

Flowball is a multi step pipeline. The last step is the visualization step and for each step you move away from visualization the level of technical prowess required increases. The steps are as follows

- Visulization (project flowerpot)
- Transformation (project hobbes)
- Data collection (multiple independant projects)

## Flowerpot

Flowerpot is a project for creating web sites. It can be used independantly of the rest of flowball. The idea is to create a high level language for creating web sites. The core of the project is a language to describe web sites with a higher level of semantics than what HTML can offer. It's not meant to support the creation of all possible web sites but is instead aiming at providing core elements for navigation, layout and input and to be extensible through node packages or react components.

## Hobbes

Hobbes is a high level language for data transformation. The vision is to make standard transformation such as calculating standard deviations, mean or median to a one liner and make it possible to chaining these transformations. The input and output to the transformations are a matrix

## Data Collectors

A data collector in the context of flowball is a service that can retrieve data from a specific data source such as Azure DevOps, Jira, World Bank, YR Weather service or some other data provider. The data collector has a set of named dataset. Each dataset is the result of transforming the data from the original structure to a matrix that can be understood by a hobbes transformation

## Documentation 

The documentation for this library is automatically generated (using the literate programming tools based on
the library) from `*.fsx` and `*.md` files in [the docs folder][2]. If you find a typo, please submit a pull request! 

- [F# Formatting: Documentation tools][3] provides more information about the library.

## Library license

The library is available under MIT license. For more information see the [License file][1] in the GitHub repository.

 [1]: https://github.com/kmd/flowball/blob/master/LICENSE.md
 [2]: https://github.com/kmd/flowball/tree/master/docs
 [3]: http://fsprojects.github.io/FSharp.Formatting/
 [4]: http://fsprojects.github.io/FSharp.Formatting/literate.html