open System.IO
#r "paket: groupref build //"
#load "./src/buildBase.fsx"

#if !FAKE
#r "netstandard"
#r "Facades/netstandard" // https://github.com/ionide/ionide-vscode-fsharp/issues/839#issuecomment-396296095
#endif

open BuildBase.Tools
open Fake.Core
open Fake.IO
open Fake.DotNet
open System

let proxySettingTemplate serviceName = 
        sprintf """
        location ~ /%s/(.*) {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            set $upstream http://%s:8085/$1;
            proxy_pass $upstream;
        }""" serviceName  serviceName

let nginxConfTemplate = sprintf """## THIS FILE IS AUTO GENERATED DO NOT EDIT
worker_processes  5;  ## Default: 1
#error_log  logs/error.log;
#pid        logs/nginx.pid;
worker_rlimit_nofile 8192;

events {
  worker_connections  4096;  ## Default: 1024
}

http {
    server {
        resolver 127.0.0.11 ipv6=off;
        listen 80;
        server_name collector collector.*;
%s
    }
}"""

let createComposeFile serviceNames =
    let indent = "   " 
    let doubleIndent = indent + indent
    let tripleIndent = doubleIndent + indent
    let collectorNetwork = "collectors"
    let depends_on = 
        String.Join("\n" + tripleIndent,
            serviceNames 
            |> Array.map(sprintf "- %s")
        )
    let services = 
        String.Join("\n" + indent,
            serviceNames
            |> Array.map(fun serviceName ->
                let networks = sprintf "networks:\n%s- %s" tripleIndent collectorNetwork
                let expose = sprintf "expose:\n%s- %d" tripleIndent 8085
                [                
                    sprintf "container_name: %s"
                    sprintf "image: kmd/%s_data_collector"
                    fun _ -> expose
                    fun _ -> networks
                 ] |> List.map(fun f ->
                    f serviceName |> sprintf "\n%s%s" doubleIndent
                 ) |> String.Concat
                |> sprintf "%s:%s" serviceName
            ))
        
    sprintf """## THIS FILE IS AUTO GENERATED DO NOT EDIT
version: '3.7'
networks:
   internal:
   %s:
services:
   collector:
      image: kmd/collector
      depends_on:
         %s
   %s""" collectorNetwork depends_on services


let buildDocker () =
    runTool "docker-compose" "build" "."


let runDocker asDaemon =
    runTool "docker-compose" ("-f docker-compose.yml -f docker-compose.overwrite.yml up" + if asDaemon then " -d" else "") "."

let collectorFile = "./src/flowerpot/Client/CollectorList.fs"
let composeFile = "./docker-compose.yml"
let srcDir = "./src"
let dataCollectorDir = Path.combine srcDir "data collectors"
let nginxCollectorDir = Path.combine dataCollectorDir "collector"
let nginxConfigFile = Path.combine nginxCollectorDir "nginx.conf"
let getBuildName (buildDir: string) = 
    (buildDir.Split([|"/";"\\"|], StringSplitOptions.RemoveEmptyEntries) |> Array.last).ToLower()

Target.create "Clean" (fun _ ->
    if File.exists composeFile then File.delete composeFile
    if File.exists nginxConfigFile then File.delete nginxConfigFile
)

Target.create "ComposeDataCollectors" (fun _ ->
    
    let serviceNames = 
        IO.Directory.GetDirectories(dataCollectorDir) 
        |> Array.filter(fun directory -> getBuildName directory <> "collector")
        |> Array.map(fun directory -> Path.combine directory "build.fsx")
        |> Array.filter IO.File.Exists
        |> Array.map(Path.getDirectory >> getBuildName)
    
    createComposeFile serviceNames
    |> File.writeString false composeFile

    
    let rec createCollectorFile = 
        function
        | [] -> []
        | x::xs -> ("\"" + x + "\"") :: (createCollectorFile xs) 

    let addSemiColons lst = String.Join (";", (lst: string list))
    
    let formatter = sprintf """module CollectorList
            let collectorList = [%s]""" 
                            
    let collectorListCode = List.ofArray serviceNames
                            |> createCollectorFile 
                            |> addSemiColons 
                            |> formatter

    System.IO.File.WriteAllText (collectorFile, collectorListCode) 

    serviceNames
    |> Array.map(proxySettingTemplate) 
    |> String.Concat
    |> nginxConfTemplate
    |> File.writeString false nginxConfigFile
)
let run asDaemon  _ = 
    let dockerCompose = async {
        runDocker asDaemon
    }

    let browser = async {
        do! Async.Sleep 5000
        8080us |> sprintf "http://localhost:%d" |> openBrowser 
    }

    let tasks =
        [ 
          yield dockerCompose
          if not asDaemon then yield browser
        ]

    tasks
    |> Async.Parallel
    |> Async.RunSynchronously
    |> ignore

open Fake.Core.TargetOperators

let createChildTarget name buildDir =
    let buildName = (buildDir |> getBuildName)
    let targetName = name + buildName
    Target.create targetName (fun _ ->
        runTool "fake" <| sprintf "build --target %s" name <| buildDir
    )
    targetName ==> name |> ignore
    targetName

Target.create "Up" (run true)
Target.create "Run" (run false)
Target.create "Kill"(fun _ ->
    runTool "docker-compose" ("-f docker-compose.yml -f docker-compose.overwrite.yml kill") "."
)

Target.create "RunAsDaemon" (run true)
Target.create "PreChildren" ignore

[
    "PreCompile"
    "Compile"
    "Test"
    "Docker"
    "Push"
] |> List.iter (fun n -> Target.create n ignore)
    
//create a build target for each build file for ease of use from thte root of the project
//execute a command like `fake build --target BuildFlowerpot` or `fake build --target BuildHobbes`
//to build just the one project including creating a new tagged doecker image
//make each of those targets a dependency for Build
let childBuilds = 
    IO.Directory.GetFiles(srcDir, "build.fsx", IO.SearchOption.AllDirectories)
    |> Array.map Path.getDirectory
childBuilds
|> Array.iter(fun buildDir ->
    let isFlowerpot = (buildDir |> getBuildName).ToLower() = "flowerpot" 
    (if isFlowerpot then 
        "PreCompile"
        ?=> createChildTarget "Compile" buildDir
     else
        "PreChildren"
        ?=> createChildTarget "Compile" buildDir
        ?=> "PreCompile"
    )
    ==> createChildTarget "Docker" buildDir
    ==> createChildTarget "Push" buildDir
    |> ignore
)
//create test targets
System.IO.Directory.GetFiles(srcDir, "*.fsproj", IO.SearchOption.AllDirectories)
|> Array.toList
|> List.filter(fun path -> 
    (path.Contains ".fable" || path.Contains ".fake") |> not
) |> List.map(fun projFile ->
    let projName = 
       projFile |> System.IO.Path.GetFileNameWithoutExtension
    let testProjName = 
        projName + ".tests"
    let testProjPath = 
        projFile.Replace(projName,testProjName).Replace("src","tests")
    testProjPath
) |> List.filter System.IO.File.Exists
|> List.iter(fun testProject ->
   let buildName = (getBuildName <| Path.GetDirectoryName testProject)
   let targetName = "Test" + buildName
   Target.create targetName (fun _ -> DotNet.test id testProject)
   "Compile" 
       ==> targetName 
       ==> "Test" 
       |> ignore
)

Target.create "install-newman" (fun _ -> 
    let packetName = "newman"
    runTool npmTool ("install -g " + packetName) "."
    )
Target.create "API-test" (fun _ ->
    let newmanTool = platformTool "newman" "newman.cmd"
    runTool newmanTool "run ./API-Test.json" ".")

"Clean"
    ==> "ComposeDataCollectors" 
    ==> "PreChildren"
    ==> "Compile"
    ==> "Test"
    ==> "Docker"
    ==> "Up"
    ==> "API-test"
    ==> "Push"

"install-newman" ==> "API-test"

Target.runOrDefaultWithArguments "Docker"