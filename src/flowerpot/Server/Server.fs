open System.IO
open Saturn
open Shared.Flowerpot
open Fable.Remoting.Server
open Fable.Remoting.Giraffe
open FSharp.Data
open FSharp.Data.HttpRequestHeaders
open Fable.FontAwesome.Free

let publicPath = Path.GetFullPath "../Client/public"
let port = 8085us


let api = {
            page =
                fun _ ->
                  async {
                      printfn "Returning front page"
                      return true
                  }
    }
let webApp =
    Remoting.createApi()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.fromValue api
    |> Remoting.buildHttpHandler

let app = application {
    url ("http://0.0.0.0:" + port.ToString() + "/")
    use_router webApp
    memory_cache
    use_static publicPath
    use_gzip
}

run app
