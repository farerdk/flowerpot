module Site
type DispatchAction = 
  | RequestData of string * string
  | ChangeType of string
type Color =
   | IsWhite      | IsBlack
   | IsBlackBis   | IsBlackTer       
   | IsDanger     | IsCustomColor of string
   | IsDark       | IsGrey           
   | IsGreyDark   | IsGreyDarker     
   | IsGreyLight  | IsGreyLighter    
   | IsInfo       | IsLight          
   | IsLink       | IsPrimary        
   | IsSuccess    | IsWarning        
   | IsWhiteBis   | IsWhiteTer       
type NavbarSubmenuItem =
    {
        IconName : string option
        Title : string
        Items : NavbarItem list
    }

and NavbarItem =
    Single of AST
    | Submenu of NavbarSubmenuItem

and NavbarElement =
   {
       Menu : NavbarItem list
       Color : Color
       Brand : AST
   }
and ContainerElement =
   {
       Items : AST list
   }
and Data =
    {
        Name : AST
        Dataset : string*string
    }
and MenuItem =
    Item of AST
    | List of AST * MenuItem list

and ColumnWidth =
   | Is1  | Is2
   | Is3  | Is4
   | Is5  | Is6
   | Is7  | Is8
   | Is9  | Is10
   | Is11 | Is12

and Column =
    {
        Width : ColumnWidth
        Content : AST list
    }
and Table =
    {
        thead : AST list
        tbody : AST list list
    }
and Visualiser =
    {
        dataset : (string*string) option
        buttons : string list
    }
and [<RequireQualifiedAccess()>] AST =
   Navbar of NavbarElement
   | Dataset of Data
   | Literal of string
   | Menu of MenuItem list
   | Breadcrumbs of AST list
   | Layout of customClass : string option * Column list 
   | Table of Table
   | Visualiser
   | Graph
   | Datasetlist of AST list
   | CollectorMenu