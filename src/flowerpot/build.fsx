open System.IO
#r "paket: groupref build //"
#load "../buildBase.fsx"

#if !FAKE
#r "netstandard"
#r "Facades/netstandard" // https://github.com/ionide/ionide-vscode-fsharp/issues/839#issuecomment-396296095
#endif

open BuildBase
open BuildBase.Tools
open Fake.Core
open Fake.IO

let serverPath = Path.getFullName "./Server"
let clientPath = Path.getFullName "./Client"
let clientDeployPath = Path.combine clientPath "deploy"
let deployDir = Path.getFullName "./deploy"

let shouldBuild buildFile pattern = 
    let latestBuild = 
        if File.Exists buildFile then
           FileInfo(".build").LastWriteTime
        else
           System.DateTime.MinValue
    let latestWrite = 
        System.IO.Directory.GetFiles(".", pattern, SearchOption.AllDirectories)
        |> Array.map(fun f -> FileInfo(f))
        |> Array.maxBy(fun f -> f.LastWriteTime)
    if latestWrite.LastWriteTime > latestBuild then
        printfn "Should build because %s (and potentially other files) were updated" latestWrite.Name
        true
    else
        false

Target.create "Clean" (fun _ ->
    if shouldBuild ".build" "*.fs" then
        [ deployDir
          clientDeployPath ]
        |> Shell.cleanDirs
)


Target.create "InstallClient" (fun _ ->
    if shouldBuild ".jsbuild" "package.json" then
        printfn "Node version:"
        runTool nodeTool "--version" "."
        printfn "Yarn version:"
        runTool yarnTool "--version" "."
        runTool yarnTool "install --frozen-lockfile" "."
        runDotNet "restore" clientPath
        if File.Exists ".build" then File.Delete ".build"
        File.WriteAllText(".jsbuild","")
)

Target.create "Build" (fun _ ->
    if shouldBuild ".build" "*.fs" then
        runDotNet "build" serverPath
        runTool yarnTool "webpack-cli -p" "."
)

Target.create "Run" (fun _ ->
    let server = async {
        runDotNet "watch run" serverPath
    }
    let client = async {
        runTool yarnTool "webpack-dev-server" "."
    }
    let browser = async {
        do! Async.Sleep 5000
        8085us |> sprintf "http://localhost:%d" |> openBrowser
    }

    let vsCodeSession = Environment.hasEnvironVar "vsCodeSession"
    let safeClientOnly = Environment.hasEnvironVar "safeClientOnly"

    let tasks =
        [ if not safeClientOnly then yield server
          yield client
          if not vsCodeSession then yield browser ]

    tasks
    |> Async.Parallel
    |> Async.RunSynchronously
    |> ignore
)

Target.create "Bundle" (fun _ ->
    if shouldBuild ".build" "*.fs" then
        let serverDir = Path.combine deployDir "Server"
        let clientDir = Path.combine deployDir "Client"
        let publicDir = Path.combine clientDir "public"

        let publishArgs = sprintf "publish -c Release -o \"%s\"" serverDir
        runDotNet publishArgs serverPath

        Shell.copyDir publicDir clientDeployPath FileFilter.allFiles
)

let imageName = "flowerpot"
Target.create "Docker" (dockerTarget imageName)   
Target.create "Push" (fun _ ->         
    let tagArgs = sprintf "tag kmd/%s runefs/%s" imageName imageName
    let pushArgs = sprintf "push runefs/%s" imageName
    runTool "docker" tagArgs "."
    runTool "docker" pushArgs "."
)

Target.create "Compile" (fun _ -> 
    if shouldBuild ".build" "*.fs" then
        File.WriteAllText(".build", "")
)

open Fake.Core.TargetOperators
"Clean"
    ==> "InstallClient"
    ==> "Build"
    ==> "Bundle" 
    ==> "Compile"

Target.runOrDefaultWithArguments "Docker"