namespace Flowerpot

module Math =

    //return m and b of the fomula y = m*x + b
    let findLineByLeastSquares xSerie ySerie =
        let length = 
            xSerie 
            |> Array.length
        
        match xSerie with
        _ when length <> (ySerie |> Array.length) -> 
            failwith "The parameters xSErie and ySerie need to have same size!" 
        | [||] ->
            failwith "Can't calculate a linear regression for an empty dataset"
        | _ ->
            let length = float length
            let sumX,sumY,sumXX,sumXY = 
               ySerie
               |> Array.zip xSerie
               |> Array.fold(fun (sumX,sumY,sumXX,sumXY) (x,y) ->

                   (sumX + x,sumY + y, sumXX + x * x, sumXY + x * y)
               ) (0.0,0.0,0.0,0.0)

            let m = (length * sumXY - sumX * sumY) / (length * sumXX - sumX * sumX)
            let b = (sumY / length) - (m * sumX) / length
            m,b