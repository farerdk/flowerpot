﻿namespace TempShared

type DataType = 
     Number
     | String
     | DateTimeOffset

type DataColumn = 
    {
        Name : string
        Values : string list
        Type : DataType
    }

type VisModel = {  
    VisualiserType: string
    Dataset : (string * string) option
    Data : DataColumn[] option
    Id : int
    GraphTypes: Map<string, string>
    xAxisIndex: int
    TransformationList : string list
    transformationName : string
}

