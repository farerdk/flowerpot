#r "paket: groupref build //"
#load "../../../buildBase.fsx"
open BuildBase.Tools

open Fake.Core
open Fake.Core.TargetOperators

let imageName = "dashboarddb"
Target.create "Docker" (dockerTarget imageName)   
Target.create "Push" (fun _ ->         
    let tagArgs = sprintf "tag kmd/%s runefs/%s" imageName imageName
    let pushArgs = sprintf "push runefs/%s" imageName
    runTool "docker" tagArgs "."
    runTool "docker" pushArgs "."
)

Target.create "Compile" ignore //required by main build script but nothing to build here
Target.runOrDefaultWithArguments "Docker"