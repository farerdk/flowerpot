﻿module DashboardDb

open Visualiser
open Fetch
open Fable.Core

let makeDB db =
    let defaultProps =
        [ RequestProperties.Method HttpMethod.PUT
        ; requestHeaders [ContentType "application/json"]]

    promise {
        try
            printfn "tried to create database %s" db
            let! res = fetch (sprintf "http://localhost:8080/dashboarddb/%s" db) defaultProps
            ()
        with _ -> 
        printfn "failed to create database %s" db
        ()
    } |> Promise.start

let create id jsonDashboard =
    promise {
        try
            let defaultProps =
                [ RequestProperties.Method HttpMethod.PUT
                ; requestHeaders [ContentType "application/json"]
                ; RequestProperties.Body (unbox(sprintf "{\"dashboard\": %s }" jsonDashboard))]

            let! result = fetch ("http://localhost:8080/dashboarddb/dashboard/" + id) defaultProps
                         
            return result.Ok
        with _ ->
            return false
    }
        
let get id =
    
    promise {
        try 
            let! res = fetch "http://localhost:8080/dashboarddb/dashboard" []
            let! txt = res.text()
            ()
        with _ ->
            ["_global_changes"; "_replicator"; "_users"; "dashboard"]
            |> List.iter (fun db -> db |> makeDB)
        
        try
            let! res = fetch (sprintf "http://localhost:8080/dashboarddb/dashboard/%s" id) []
            let! txt = res.text()
            return txt
        with e ->
            printfn "Dashboard with id: %s didn't exist" id
            create id "[]" |> ignore
            return "\"dashboard\": []"
    }

let update id jsonDashboard =
    let maxRetries = 5
    let rec inner count =
        promise {
            let! prevDashboard = get id
            let rev = Serializer.revDecoder prevDashboard
            
            let defaultProps =
                [ RequestProperties.Method HttpMethod.PUT
                ; requestHeaders [ContentType "application/json"; IfMatch rev]
                ; RequestProperties.Body (unbox(sprintf "{\"dashboard\": %s }" jsonDashboard))]

            let! result = GlobalFetch.fetch(RequestInfo.Url ("http://localhost:8080/dashboarddb/dashboard/" + id), requestProps defaultProps)

            if result.Ok then return true
            elif result.Status = 409 && maxRetries > count then return! inner (count + 1)
            else 
                printfn "failed to update dashboard with id: %s" id
                return false
        }
    inner 1
let getDashboard id =
    get id |> Async.AwaitPromise 
let saveDashboard (id, json) =
    update id json |> Async.AwaitPromise