namespace Flowerpot

open Shared.Hobbes
open Fulma

type ChartType = 
    Line
    | ScatterPlot
    | Bar
    | Column
    | Area
    | Pie
    //TODO feels like a hack to include it here, shuold probably be moved to somewhere else
    | TrendLine
    with
        override x.ToString() = 
            match x with
            Line -> "line"
            | ScatterPlot -> "scatterplot"
            | Bar -> "bar"
            | Column -> "column"
            | Area -> "area"
            | Pie -> "pie"
            //TODO feels like a hack to include it here, shuold probably be moved to somewhere else
            | TrendLine -> "trendline"
        static member Parse s =
            match s with
            "line" -> Line
            | "scatterplot" -> ScatterPlot
            | "bar" -> Bar
            | "column" -> Column
            | "area" -> Area
            | "pie" -> Pie
            | "trendline" -> TrendLine
            | _ -> failwithf "Don't know how to parse %s as a ChartType" s
type ColumnSerie = 
    {
        HasTrendline : bool
        DataColumns : DataColumn []
        ChartType : ChartType
        ToolTip : string option
    }

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
module ColumnSerie =
    let length (serie: ColumnSerie) =
        serie.DataColumns.[0].Values.Length
    let isNumeric (serie : ColumnSerie) =
        serie.DataColumns
        |> Array.head
        |> DataColumn.isNumeric
    let combinedSerie (serie : ColumnSerie) = 
        match serie.DataColumns with
        [||] -> failwith "Can't combine an empty array of data columns"
        | [|d|] -> d 
        | columns ->
            columns
            |> Array.reduce (fun dc1 dc2 ->
               if dc1.Name <> dc2.Name || dc1.Type <> dc2.Type then failwith "Incompatible DataColumns"
               {
                   dc1 with
                       Values =
                          dc1.Values
                          |> List.zip dc2.Values
                          |> List.map(fun (a,b) -> float(a) + float(b) |> string)
               }
            )
    let columns (serie : ColumnSerie) =
        serie.DataColumns
    let filledColumns (serie : ColumnSerie) =
        let noneEmpty = 
            serie.DataColumns
            |> Array.filter(fun c ->
               c.Values |> List.isEmpty |> not
            )
        noneEmpty
    let tryFindColumn name (serie : ColumnSerie) =
        serie.DataColumns
        |> Array.tryFind(fun dc -> dc.Name = name)
    let updateColumn column serie =
        {
            serie with
                DataColumns = 
                    serie.DataColumns
                    |> Array.map(fun c ->
                        if c.Name = column.Name then column
                        else c
                    )
        }
