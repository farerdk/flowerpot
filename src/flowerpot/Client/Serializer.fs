﻿module Serializer
open Thoth.Json
open Visualiser
open Flowerpot

let stackEncoder stack =
    Encode.object
        [ "Stacks", Encode.list (Map.fold(fun s k v -> s @ [Encode.tuple2 Encode.string Encode.string (k, (string v))]) [] stack.Stacks)
          "Stacking", (Encode.tuple2 Encode.string Encode.bool) stack.Stacking 
          "StackId", Encode.int stack.StackId
          "ColorMap", Encode.list (Map.fold (fun s k v -> s @ [Encode.tuple2 Encode.string Encode.string (k, v)]) [] stack.ColorMap)
        ]
        
let seriesEncoder series = 
    Encode.array(Array.map(fun (serie : Flowerpot.ColumnSerie) ->
        Encode.object [
                "ChartType", serie.ChartType.ToString() |> Encode.string 
                "ColumnNames", serie.DataColumns |> Array.map (fun c -> c.Name |> Encode.string) |> Encode.array
                "HasTrendLine", Encode.bool serie.HasTrendline
                "ToolTip", Encode.option (Encode.string) serie.ToolTip
        ]) series
    )

let visModelEncoder visModel = 
    Encode.object
        [ "VisualiserType", visModel.VisualiserType.ToString() |> Encode.string 
          "Dataset", Encode.option (Encode.tuple2 Encode.string Encode.string) visModel.Dataset
          "Id", Encode.int visModel.Id
          "DataSeries", Encode.option seriesEncoder visModel.DataSeries 
          "xAxisIndex", Encode.int visModel.xAxisIndex
          "TransformationList", (List.map Encode.string visModel.transformationList) |> Encode.list
        ]

let visModelMapEncoder visModel =
    Encode.list (Map.fold (fun s _ v -> s @ [visModelEncoder v]) [] visModel)

let dataSeriesDecoder = 
    (Decode.array 
           (Decode.map4 
               (fun chartType columnNames hasTrendLine toolTip ->
                   {
                           ChartType = chartType |> Flowerpot.ChartType.Parse
                           DataColumns = 
                               columnNames
                               |> Array.map(fun name ->
                                       { 
                                           Name = name
                                           Values = []
                                           Type = Shared.Hobbes.DataType.String
                                       }
                               )
                           HasTrendline = hasTrendLine
                           ToolTip = toolTip
                   }
               )
              (Decode.field "ChartType" Decode.string)
              (Decode.field "ColumnNames" (Decode.array Decode.string))
              (Decode.field "HasTrendLine" Decode.bool)
              (Decode.optional "ToolTip" Decode.string)
           )
        )   

let visModelDecoder = 
        Decode.map6 (fun visualiserType dataset columnSeries id xIndex transList ->
                { VisualiserType = visualiserType |> VisualiserType.Parse
                  Dataset = dataset
                  DataSeries = columnSeries
                  Rows = null
                  DataObject = null
                  IsStacking = None
                  Id = id
                  xAxisIndex = xIndex
                  transformationList = transList
                  ToolboxMode = false
                  Size = None} : VisModel)
                 
            (Decode.field "VisualiserType" Decode.string )
            (Decode.field "Dataset" (Decode.option (Decode.tuple2 Decode.string Decode.string)))
            (Decode.optional "DataSeries" dataSeriesDecoder)
            (Decode.field "Id" Decode.int)
            (Decode.field "xAxisIndex" (Decode.int))
            (Decode.field "TransformationList" (Decode.list Decode.string))


let revDecoder json = 
    match Decode.fromString (Decode.field "_rev" Decode.string) json with
    | Ok(rev) -> rev
    | Error(e) -> failwithf "%A" e

let getDecodedModel json = 
    match Decode.fromString (Decode.field "dashboard" (Decode.list visModelDecoder) ) json with
    | Ok(visModelList) -> List.fold(fun s vis -> Map.add vis.Id vis s) Map.empty visModelList
    | Error(e) -> failwithf "%A" e

let encodeModel model = visModelMapEncoder model |> Encode.toString 0
