﻿module ToolboxVisualiser
open Browser
open Elmish
open Elmish.React

open Fable.React
open Fable.React.Props
open Shared.Flowerpot
open Shared.Hobbes
open Fulma
open Shared.Hobbes
open Fable.Import

open Fable.Recharts
module P = Fable.React.Props
open Fable.Recharts.Props
open Fable.Core.JsInterop
open Fable.FontAwesome
open System
open Visualiser
open Fable.FontAwesome.Free

type ToolboxVisualiserModel = {
        VisModel : VisModel
        isShowingTransformation : bool
        isClosingTransformationOverlay: bool
        transformationStartValueName : string
        transformationStartValueBody : string Option
        validatedTransformation : string Option
        transformationName : string Option 
        transformationBody : string Option
        transformationIndex : int
        isDragging : string Option
    }


type ExToolBoxVisualiseMsg =
| ExSetIsShowingTransformationModal of string*int
| ExRequestClosingTransformationModal
| ExSetTransformationName of string
| ExSetTransformationBody of string
| ExCloseAndSave of string*string*int
| ExCloseAndDiscard
| ExCloseAcceptModal
| ExDisableDragging

type ToolBoxVisualiserMsg =
| UpdateVisModelMsg of VisModel
| SetWantToCloseTransformation
| SetIsShowingTransformation of bool*string*int
| SetTransformationValidateWith of string Option
| SetLoadedTransformationBody of string
| SetTransformationName of string
| SetTransformationBody of string
| SetCloseTransformationOverlay
| SetCloseAcceptModal
| SetDragging of string
| DisableDragging


let init visModel : ToolboxVisualiserModel = 
    {
        VisModel = visModel
        isShowingTransformation= false
        isClosingTransformationOverlay = false
        transformationStartValueName = ""
        validatedTransformation = None
        transformationStartValueBody = None
        transformationName = None
        transformationBody = None
        transformationIndex = 0
        isDragging = None
    }

        
let update (msg) (currentModel : ToolboxVisualiserModel) : ToolboxVisualiserModel * Cmd<ToolBoxVisualiserMsg>=
    let model =
        match msg with
        | UpdateVisModelMsg visModel    ->  debugPrint "toolboxvis updated with %A" visModel.Dataset
                                            {currentModel with VisModel = visModel}
        | SetWantToCloseTransformation ->
            match currentModel.transformationName, currentModel.transformationBody with
            | None, None -> {currentModel with isShowingTransformation = false}
            | _, _ -> {currentModel with isClosingTransformationOverlay = true}
        | SetIsShowingTransformation (isShowing, name, index) ->
            let nextModel = {currentModel with isShowingTransformation = isShowing
                                                                         transformationStartValueName = name
                                                                         transformationIndex = index}
            if not isShowing then nextModel else {nextModel with transformationName = None;
                                                                 transformationBody = None
                                                                 transformationStartValueBody = None}
        | SetLoadedTransformationBody value -> {currentModel with transformationStartValueBody = Some value}
        | SetTransformationValidateWith value -> {currentModel with validatedTransformation = value}
        | SetTransformationName value -> {currentModel with transformationName = Some value}
        | SetTransformationBody value -> {currentModel with transformationBody = Some value}
        | SetCloseTransformationOverlay -> {currentModel with isClosingTransformationOverlay = false
                                                              isShowingTransformation = false
                                                              transformationStartValueBody = None
                                                              transformationName = None
                                                              transformationBody = None
                                                              validatedTransformation = None}
        | SetCloseAcceptModal -> {currentModel with isClosingTransformationOverlay = false}
        | SetDragging dragName -> {currentModel with isDragging = Some dragName}
        | DisableDragging -> {currentModel with isDragging = None}
    model, Cmd.none

let printDataHeaders model =
    match model.VisModel |> VisModel.filledColumns with
    | [||] -> [str "no data"]
    | data -> [ 
            yield Table.table [ Table.CustomClass "DataHeadersTable"; Table.IsBordered; Table.IsNarrow; Table.IsStriped ]
                [ thead [ ]
                    [ tr [ ] 
                        [ th [ ] [ str "Dataset Headers:" ] ] ]
                  tbody [ ]
                    (   data
                        |> Array.take(min 100 data.Length)
                        |> Array.map(fun column -> (tr [ ] [ td [] [ str column.Name ] ]) ))
                ]
        ]
    

let basicModal isActive togleDisplay content =
    Modal.modal [ Modal.IsActive isActive ]
        [
            Modal.background [ Props [ OnClick togleDisplay ] ] [ ]
            Modal.content [ ] [ div[ClassName "modalContent"][ Box.box' [ ] [ content ] ]]
            Modal.close [ Modal.Close.Size IsLarge; Modal.Close.OnClick togleDisplay ] [ ]
        ]
let transformationModal model dispatch togleDisplay =
    let textArea = 
        match model.transformationStartValueBody with
        | None -> str ""
        | Some body -> Control.div [ Control.IsLoading false ] [ Textarea.textarea [
                                                    Textarea.CustomClass "ModalTextArea";
                                                    Textarea.DefaultValue body;
                                                    Textarea.OnChange (fun e -> e.Value |> ExSetTransformationBody |> dispatch) ] [ ] ]

    let textAreaErrorMessage =
        match model.validatedTransformation with
        | None       -> div [ClassName "TransNotErrorMsg"] [str "The transformation parses and compiles correctly"]
        | Some error -> div [ClassName "TransErrorMsg"]    [str error]

    let content =
        match model.isShowingTransformation with
        | false -> str ""
        | true -> 
            div [ ] [
              Label.label [ ] [ str "File name" ]
              Control.div [ ] [ Input.text [ Input.Placeholder "Ex: bob";
                                  Input.DefaultValue (model.transformationStartValueName);
                                  Input.Size ISize.IsLarge;
                                  Input.OnChange (fun e -> e.Value |> ExSetTransformationName |> dispatch)] ]
          
              Label.label [ ] [ str "Message" ]
              textArea
              textAreaErrorMessage
            ]
    content
    |> basicModal model.isShowingTransformation togleDisplay
let acceptModal model dispatch visDispatch togleDisplay name body =
    let labels = 
        match model.validatedTransformation with
        | None -> 
            div[][
              Label.label [ ] [ str ("Do you want to save changes to "+name+"?") ]
              Label.label [ ] [ str "Your changes will lost if you don't save them." ]
          ]
        | Some _ -> div[][
              Label.label [ ] [ str ("There were an error discard or go back") ]
          ]
    let buttons = 
        match model.validatedTransformation with
        | None -> 
            div[][
              Button.button [Button.Size ISize.IsSmall; Button.Color IColor.IsWhite; Button.OnClick (fun _ ->
                                                                                                        (name, body, model.VisModel.Id) |> ExCloseAndSave |> dispatch;
                                                                                                        (name, model.transformationIndex)|> ExTransformationOverwrite |> Visualiser.combineToTuple model.VisModel.Id |>visDispatch) ]
                [str "Save"]
              Button.button [Button.Size ISize.IsSmall; Button.Color IColor.IsWhite; Button.OnClick (fun _ -> ExCloseAndDiscard |> dispatch) ]
                [str "Don't save"]
              Button.button [Button.Size ISize.IsSmall; Button.Color IColor.IsWhite; Button.OnClick (fun _ -> ExCloseAcceptModal |> dispatch) ]
                [str "Cancel"]
          ]
        | Some _ ->
            div[][
              Button.button [Button.Size ISize.IsSmall; Button.Color IColor.IsWhite; Button.OnClick (fun _ -> ExCloseAndDiscard |> dispatch) ]
                [str "Don't save"]
              Button.button [Button.Size ISize.IsSmall; Button.Color IColor.IsWhite; Button.OnClick (fun _ -> ExCloseAcceptModal |> dispatch) ]
                [str "Cancel"]
          ]

    let content =
        div [ ] [
          labels
          buttons
         
        ]
    content
    |> basicModal model.isClosingTransformationOverlay togleDisplay

let removeTransformationModal dispatch = (fun _ -> ExRequestClosingTransformationModal |> dispatch)
let removeConfirmModal dispatch = (fun _ -> ExCloseAcceptModal |> dispatch)
let activateTransformationModal dispatch transName index = (fun _ -> (transName, index) |> ExSetIsShowingTransformationModal |> dispatch)

let view (model : ToolboxVisualiserModel) (dispatch : ExToolBoxVisualiseMsg -> unit) visDispatch = 

    let model = {model with VisModel = {model.VisModel with ToolboxMode = true}}
    debugPrint "Create ToolBox %A" model.VisModel.transformationList
    let preventDefault(ev:Types.DragEvent) = 
        ev.preventDefault()
        ev.target?style?rsor <- "grabbing"
    let getData (event:Types.DragEvent) =
        match event.dataTransfer.getData "DataCollector" with
        | "" -> ()
        | dataset ->
            debugPrint "Visualiser got data : %A" dataset
            let collectorAndSet = dataset.Split(' ');
            (collectorAndSet.[0], collectorAndSet.[1], model.VisModel.transformationList)
            |> ExRequestDataset
            |> Visualiser.combineToTuple model.VisModel.Id
            |> visDispatch
    let getTransformation (event:Types.DragEvent) =
        match event.dataTransfer.getData "Transformations" with
            | "" -> ()
            | transformationName -> 
                debugPrint "Visualiser got transformation : %A" transformationName
                transformationName
                |> ExTransformationChosen
                |> Visualiser.combineToTuple model.VisModel.Id
                |> visDispatch

    let removeTransformation index = (fun _ -> index
                                               |> ExTransformationRemoved
                                               |> Visualiser.combineToTuple model.VisModel.Id  
                                               |> visDispatch)

    let removeAllTransformations =
        (fun _ -> ExAllTransformationsRemoved
                  |> Visualiser.combineToTuple model.VisModel.Id
                  |> visDispatch)

    let createTranformationBox i s =
        div [Class "TransBox"] [
        span[] [str s 
                Button.button [(Button.OnClick(activateTransformationModal dispatch s i));
                                Button.Size ISize.IsSmall; Button.Color IColor.IsWhite ]
                    [Icon.icon [] [ Fa.i [ Fa.Solid.Wrench ][ ] ]]
                Button.button [(Button.OnClick(removeTransformation i));
                                Button.Size ISize.IsSmall; Button.Color IColor.IsWhite ]
                    [Icon.icon [] [ Fa.i [ Fa.Solid.WindowClose ][ ] ]]
        ] 
    ]

    let createTransformationBoxes lst =
        let rec aux acc lst i =
            match lst with
            | [] -> []
            | [x] -> acc @ [createTranformationBox i x]
            | x::xt -> aux (acc @ [createTranformationBox i x;  (Icon.icon [Icon.CustomClass "ArrowIcon"] [ Fa.i [ Fa.Solid.ArrowRight ][ ] ]) ]) xt (i+1)
        aux [] lst 0

    
    let createAcceptModal =
        let defaultBodyValue = match model.transformationStartValueBody with | None -> "" | Some body -> body
        match model.transformationName , model.transformationBody with
        | (Some name, Some body) -> acceptModal model dispatch visDispatch (removeConfirmModal dispatch) name body
        | Some name, None -> acceptModal model dispatch visDispatch (removeConfirmModal dispatch) name defaultBodyValue
        | None, Some body -> acceptModal model dispatch visDispatch (removeConfirmModal dispatch) model.transformationStartValueName body
        | None, None -> acceptModal model dispatch visDispatch (removeConfirmModal dispatch) model.transformationStartValueName  defaultBodyValue
    

    let createDropDownDiv dispatch className ondrop content =
       div [   OnDragOver preventDefault
               OnDragEnter preventDefault
               OnDragLeave preventDefault
               OnDrop (fun e -> ExDisableDragging |> dispatch; e|> ondrop)
               Class className 
       ] content
    let draggingMenu dispatch =
        match model.isDragging with
        | None -> str ""
        | Some _ -> createDropDownDiv dispatch "GrayedOutDiv" ignore []
    let createDropDownDivContainer model className width draggableTargetName content =
       let content = 
           match model.isDragging with
           | None -> content
           | Some v ->
                debugPrint "dragging %s" v
                debugPrint "draggableTargetName: %A" draggableTargetName
                if v <> draggableTargetName then content else [div[Class ("DraggContainer"+draggableTargetName)] content ]
       Column.column [Column.CustomClass className; Column.Width (Screen.All, width)] content
       
    div [ ] [
        
            Container.container [ Container.CustomClass "ToolBoxVisualiser"  ] [
                Columns.columns [Columns.IsVCentered] [
                    Column.column [Column.CustomClass "OneHundredWidth"] [
                        Tile.tile [Tile.CustomClass "OneHundredWidth"] [
                           Box.box' [CustomClass "OneHundredWidth" ] [
                                Visualiser.view model.VisModel (visDispatch)
                    ]
                ]    
                    ]
                ]
                
            ]
            Container.container [ Container.CustomClass "ToolBoxDataSetAndTransformation"] [
                Columns.columns [Columns.IsVCentered] [
            
                
                [
                    Columns.columns [Columns.CustomClass "ThreeHundredMinHeight"] [
                        [
                            [
                                div [Class "ToolboxTitle"] [str "Dataset"]
                                div [Class "DataHeaders"] (printDataHeaders model) ]
                            |>createDropDownDiv dispatch "ToolboxVisualiserDataset" getData ]
                        |> createDropDownDivContainer model "NoRightPadding" Column.IsTwoFifths "DataCollector"
                    
                        [
                            [
                                [
                                    str "Transformation" 
                                    Button.button [(Button.OnClick(removeAllTransformations));
                                                   Button.Size ISize.IsSmall; Button.Color IColor.IsWhite ]
                                       [Icon.icon [] [ Fa.i [ Fa.Solid.WindowClose ][ ] ]]]
                                |> div [Class "ToolboxTitleTrans"]
                                div[ Class "TransDiv"](model.VisModel.transformationList |> createTransformationBoxes)]
                            |>createDropDownDiv dispatch "ToolboxVisualiserTransformation" getTransformation   ]
                        |> createDropDownDivContainer model "NoLeftPadding" Column.IsThreeFifths "Transformations" 

                    ]
                ] |> Box.box' [CustomClass "BoxWidthOneHundred"] 
            ]
            ]
            transformationModal model dispatch (removeTransformationModal dispatch)
            createAcceptModal
            draggingMenu dispatch 
    ]
    
       
    