﻿module CollectorMenu
open Elmish
 
open Fulma

open Fable.React
open Fable.React.Props
open System
open Fable.React.ReactiveComponents
open Browser

open Fable.FontAwesome
open Fable.FontAwesome.Free

type MenuModel = {
    Datasets: Map<string, string[]> option
    MenuType: string
    ActiveMenu: string
}

type ExMenuMsg = 
| ExActiveMenuMsg of string * string
| ExSetDragging of string

type MenuMsg =
| MenuDataMsg of Map<string,string[]> option
| UpdateActiveMenuMsg of string

let init menuType : MenuModel = { Datasets = None; MenuType = menuType; ActiveMenu = ""}

let update msg currentModel : MenuModel * Cmd<MenuMsg> =
    match msg with
    | MenuDataMsg(datasets) ->
        match datasets with 
        | None -> currentModel, Cmd.none
        | Some _ -> 
            let nextModel = { currentModel with Datasets = datasets }
            nextModel, Cmd.none
    | UpdateActiveMenuMsg(menu) ->
        let nextActiveMenu = menu 
        let nextModel = { currentModel with ActiveMenu = (if currentModel.ActiveMenu = menu then "" else nextActiveMenu)}
        nextModel, Cmd.none
let menuItem dispatch model menuLable menuLableItem =
    let setData (event:Types.DragEvent) =
        match model.MenuType with
        | "DataCollector"   -> 
            model.MenuType |> ExSetDragging |> dispatch
            (event.dataTransfer.setData (model.MenuType, menuLable + " " + menuLableItem) |> ignore)
        | "Transformations" -> 
            model.MenuType |> ExSetDragging |> dispatch
            (event.dataTransfer.setData (model.MenuType, menuLableItem) |> ignore)
        | _                 ->  ()
    Menu.Item.li [ Menu.Item.IsActive false ]
       [ div [ ClassName "DragMe"; Draggable true; OnDragStart setData] [str menuLableItem]]

let createCollapseButton dispatch menuType v k active : ReactElement option = 
    if not (List.isEmpty v) 
    then Some(Button.button[Button.OnClick(fun _ -> dispatch <| ExActiveMenuMsg (k, menuType)); 
                            Button.Size ISize.IsSmall; Button.Color IColor.IsWhite] 
                [Icon.icon [] [ Fa.i [ (if active then Fa.Solid.AngleDown else Fa.Solid.AngleUp) ][ ] ]])
    else None

let makeMenuItems dispatch model k v = v |> Array.map(fun name -> menuItem dispatch model k name) |> Array.toList
let makeMenuContent dispatch model k v =
    let active = (k = model.ActiveMenu)
    let button = createCollapseButton dispatch model.MenuType v k active
    [
        yield Menu.label [] [ p [ClassName "MenuTitle"] [yield str k; 
                                    if button.IsSome then yield button.Value else ()] ]
        
        if active then yield Menu.list [ ] (makeMenuItems dispatch model k (Array.ofList v)) else ()
       
    ]

let view model dispatch  =
    match model.Datasets with
        | None     -> str "No datasets"
        | Some map ->
            map
            |> Map.toList
            |> List.fold (fun acc (k,v) -> acc @ (makeMenuContent dispatch model k (Array.toList v))) []
            |> Menu.menu []