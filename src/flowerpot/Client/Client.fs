module Client

open Elmish
open Elmish.React
open Elmish.Navigation
open Elmish.UrlParser
open Browser
open Fulma

open Fable.React
open Fable.Recharts
open Fable.Recharts.Props

module P = Fable.React.Props

open Shared.Flowerpot
open Shared.Hobbes
open Visualiser
open ToolboxVisualiser
open Toolbox
open CollectorList
open CollectorMenu
open Fulma
open Thoth.Json
open Shared.Flowerpot
open Shared.Hobbes
open Fable.FontAwesome
open Fable.FontAwesome.Free
open Fable.React
open Fable.FontAwesome
open Visualiser

module Server =

    open Shared
    open Fable.Remoting.Client

    // The model holds data that you want to keep track of while the application is running
    // in this case, we are keeping track of an abstract syntax tree representing the page layout
    // we mark it as optional, because initially it will not be available from the client
    // the initial value will be requested from server
    
    /// A proxy you can use to talk to server directly for page data ie AST
    let pageService : IPageService =
      Remoting.createApi()
      |> Remoting.withRouteBuilder Route.builder
      |> Remoting.buildProxy<IPageService>

    /// A proxy you can use to talk to server directly when requesting data from the hobbes server
    let dataService : IDataService =
      Remoting.createApi()
      |> Remoting.withRouteBuilder Route.builder
      |> Remoting.buildProxy<IDataService>

type Model = {     VisModelMap: Map<int,VisModel>
                   ToolboxModel: Toolbox.ToolboxModel 
                   ToolboxOverlay: bool
                   FullScreenId: int}
type ExClientMsg =
| ExOpenFullScreen of int

type Msg =
| DataLoaded of Result<DataColumn[]*int*(string*string),exn>
| TransformationLoaded of Result<string,exn>
| TransformationSaved of Result<unit,exn>*int
| TransformationValidated of Result<string Option, exn>
| RequestDataset of Result<string*string*int, exn>
| LoadDataFromDatabase of Result<DataColumn[]*int*(string*string),exn>
| TransformationsLoaded of Result<string list, exn>
| InitialTransLoaded of Result<string list, exn>
| RequestDatasetMap of Result<unit, exn>
| DatasetMapLoaded of Result<Map<string, string[]>, exn>
| ExVisualiserMsg of ExVisMsg
| VisualiserMsg of VisMsg
| TBVisualiserMsg of ToolBoxVisualiserMsg
| ExCollectorMsg of ExMenuMsg
| ExToolBoxVisualiseMsg of ExToolBoxVisualiseMsg
| CollectorMsg of MenuMsg
| ExToolBoxMsg of ExToolBoxMsg
| ToolBoxMsg of ToolBoxMsg
| AddVisualiser
| ClientMsg of ExClientMsg
| RemoveAll
| SaveDataMsg
| LoadUserDataMsg
| UserDataLoadedMsg of Result<string,exn>
| DataSavedMsg of Result<bool, exn>
| InitialDataLoaded of Result<Map<int, VisModel>,exn>

type Column = { Width : Column.ISize; Content : ReactElement list}

type NavbarSubmenuItem = {IconName : string option; Title : string; Items : NavbarItem list}
and NavbarItem =
    Single of string
    | Submenu of NavbarSubmenuItem
type NavbarElement = { Menu : NavbarItem list; Color : IColor; Brand : string}

//This is a hardcoded DataColumn[] to test displayTable functionality without connecting to Hoppes
let createDataColumnArray =
    let column1 = {Name = "Head1"; Values = ["Row1.1"; "Row1.2"]; Type = DataType.String}
    let column2 = {Name = "Head2"; Values = ["Row2.1"; "Row2.2"]; Type = DataType.String}
    [|column1; column2|]
   
let requestData (collector, set, (transformationList : string list), id) =
     async {
         try 
             let! returnedData = Server.dataService.data collector set transformationList
             return
                 match returnedData with
                 | [||] -> returnedData, id, (collector, set)
                 | array ->
                        let valueLenght = array.[0].Values.Length
                        debugPrint "valueLenght: %A" valueLenght
                        if array |> Array.exists (fun l -> valueLenght <> l.Values.Length)
                        then returnedData |> Array.map (fun l -> {l with Values = [] }), id, (collector, set)
                        else returnedData, id, (collector, set)
         with _ -> 
             return createDataColumnArray, id, (collector, set)
     }

let requestTransformations () : Async<string list> =
     async {
         try 
             let! returnedTransformations = Server.dataService.transformations ()
             return returnedTransformations  
         with _ -> 
             return []
     }

let requestTransformationText name : Async<string> =
     async {
         try 
             let! returnedTransformations = Server.dataService.readFile name
             return returnedTransformations  
         with _ -> 
             return ""
     }
     
let requestWriteTransformation (name, body) : Async<unit> =
    async {
        try 
            let! returnedTransformations = Server.dataService.saveFile name body
            return returnedTransformations  
        with e ->
            eprintfn "Save transformation failed with: %s" e.Message
            return ()
    }
     
let validateTransformationBody (body : string) : Async<string Option> =
    async {
        try 
            let! validateTransformation = Server.dataService.validateTransformation body
            return validateTransformation  
        with e ->
            eprintfn "Validate transformation failed with: %s" e.Message
            return () |> e.ToString |> Some
    }
let getDatasetMap () =
    async {
        try
            return! Server.dataService.dataset collectorList
        with e ->
            eprintf "Get datasets failed with: %s" e.Message
            return Map.empty
    }

let listToTupleList lst =
    match lst with
    [] -> []
    | prev::tail -> 
        tail
        |> List.fold(fun (acc,prev) l -> 
            let newList =
                (prev,l)::acc
            newList,l
        ) ([],prev)
        |> fst
        |> List.rev

let getQueries () =
    let path = Dom.window.location.href
    if String.exists(fun x -> x = '?') path then 
        let splitPath = path.Split '?'
        splitPath.[1].Split '=' |> Array.toList |> listToTupleList |> Map.ofList 
    else 
        Map.empty
 
let makeTiles dispatch lst =
    let rec aux lst = 
        match lst with
        | []              -> [[]]
        | [x]             -> [[x]]
        | x1::[x2]        -> [[x1; x2]]
        | x1::x2::x3::lst       -> [x1; x2; x3] :: aux lst
    aux lst
    |> List.map (fun tiles -> 
                        tiles
                        |> List.map (fun (_, vis) ->
                                        Tile.ancestor [ Tile.Props [ P.Style [ Props.CSSProp.Margin "1px" ]] ] [
                                        Tile.parent [] [
                                        Tile.child [ ][ 
                                            Box.box' [ Common.Props [ P.Style [ Props.CSSProp.Width "100%"; 
                                                                                Props.CSSProp.Height "100%";
                                                                                ] ]  ] [
                                                Visualiser.view vis (ExVisualiserMsg >> dispatch)
                                            ]
                                            ]
                                        ]
                        ])
                      
                        |> Tile.tile [ ]
    )
let getId () = 
    let id = Map.tryFind "id" (getQueries ())
    if id.IsSome then id.Value else "develop"


let asyncCmd func param msg =
    Cmd.OfAsync.either
        func
        param
        (Ok >> msg)
        (Error >> msg)
    
// defines the initial state and initial command (= side-effect) of the application
let init () : Model * Cmd<Msg> =
    let initialModel = { VisModelMap = Map.empty
                         ToolboxModel = Toolbox.init ()
                         ToolboxOverlay = false
                         FullScreenId = -1}
    let loadUserPage = asyncCmd DashboardDb.getDashboard (getId()) UserDataLoadedMsg
    
    let requestDatasetMapCmd = asyncCmd getDatasetMap () DatasetMapLoaded

    let loadTransformationCmd = asyncCmd requestTransformations () InitialTransLoaded
        
    let loadCountCmd = Cmd.batch [requestDatasetMapCmd; loadTransformationCmd; loadUserPage] 
            
    initialModel, loadCountCmd
 
let saveData currentModel =
    let json = Serializer.encodeModel currentModel.VisModelMap
    currentModel, asyncCmd DashboardDb.saveDashboard (getId(), json) DataSavedMsg


// The update function computes the next state of the application based on the current state and the incoming events/messages
// It can also run side-effects (encoded as commands) like calling the server via Http.
// these commands in turn, can dispatch messages to which the update function will react.

let updateToolBoxVisualiserModel msg currentModel =
    {currentModel with
        ToolboxModel = { currentModel.ToolboxModel with
                            ToolboxVisualiserModel =  fst (ToolboxVisualiser.update msg currentModel.ToolboxModel.ToolboxVisualiserModel)  }}

let HandleAddVisualiser currentModel =
    let highestId = Map.toList currentModel.VisModelMap 
                    |> List.fold (fun highestId (currentId, _) -> if currentId > highestId then currentId else highestId ) 0
    let nextModel = { currentModel with 
                        VisModelMap  = Map.add (highestId + 1) (Visualiser.init (highestId + 1)) currentModel.VisModelMap}
    nextModel, asyncCmd requestTransformations () TransformationsLoaded, true

let HandleClientMsg msg currentModel =
    match msg with
    | ExOpenFullScreen id -> {currentModel with FullScreenId = id }, Cmd.none, false
    

let HandleRemoveAll currentModel =
    let nextModel = { currentModel with VisModelMap = Map.empty }
    nextModel, Cmd.none, true

let HandleSaveDataMsg currentModel =
    let json = Serializer.encodeModel currentModel.VisModelMap
    currentModel, asyncCmd DashboardDb.saveDashboard (getId(), json) DataSavedMsg, false
    
let HandleDataSavedMsg result currentModel =
    match result with
            | Ok(bol) -> if bol then 
                              debugPrint "Saved!%s" "" 
                         else debugPrint "Not saved%s" ""
                         currentModel, Cmd.none, false
            | Error(e) -> eprintf "%A" e
                          currentModel, Cmd.none, false
   
let HandleLoadUserDataMsg currentModel =
    currentModel, asyncCmd DashboardDb.getDashboard (getId()) UserDataLoadedMsg, false
 
let HandleExVisualiserMsg (msg, visId) currentModel =
    let model, cmd, shouldSave =
        match msg with
        | ExRequestDataset(collector, set, transformationList) ->
            let visModel = currentModel.VisModelMap |> Map.find visId
            let model,cmd = Visualiser.update(Visualiser.DataSetMsg <| (collector, set)) visModel
            let nextMap = currentModel.VisModelMap |> Map.add visId model
            let nextModel = {currentModel with VisModelMap = nextMap}
            nextModel, Cmd.batch [asyncCmd requestData (collector, set, transformationList, visId) DataLoaded
                                  Cmd.map VisualiserMsg cmd], true
        | ExStacks(map) ->
            let model, cmd = 
                match currentModel.VisModelMap
                      |> Map.tryFind visId with
                Some v ->
                   v
                   |> Visualiser.update (Visualiser.StacksMsg <| map)
                | None -> 
                    debugPrint "Couldn't find visModel with id: %d" visId
                    Visualiser.init visId,Cmd.none

            let nextMap = currentModel.VisModelMap |> Map.add visId model
            let nextModel = {currentModel with VisModelMap = nextMap}
            nextModel, Cmd.map VisualiserMsg cmd, true
        | ExStacking(index) -> 
            let model, cmd = Visualiser.update(Visualiser.StartStacking(index)) (Map.find visId currentModel.VisModelMap)
            let nextMap = Map.add visId model currentModel.VisModelMap
            let nextModel = {currentModel with VisModelMap = nextMap}
            nextModel, Cmd.map VisualiserMsg cmd, true
        | ExVisualiserType(_type) -> 
            let visModel = currentModel.VisModelMap |> Map.find visId
            let model,cmd = Visualiser.update(Visualiser.TypeMsg <| _type) visModel
            let nextMap = currentModel.VisModelMap |> Map.add visId model
            let nextModel = {currentModel with VisModelMap = nextMap}
            nextModel, Cmd.map VisualiserMsg cmd, true
        | ExDeleteMsg ->
            let nextMap = Map.remove visId currentModel.VisModelMap
            let nextModel = { currentModel with VisModelMap = nextMap
                                                FullScreenId = -1}
            nextModel, Cmd.none, true
        | ExGraphChange (name, chartType) -> 
            let model,cmd = Visualiser.update(Visualiser.GraphChangeMsg(name,chartType)) (currentModel.VisModelMap |> Map.find visId )
            let nextMap = Map.add visId model currentModel.VisModelMap
            let nextModel = {currentModel with VisModelMap = nextMap}
            nextModel, Cmd.map VisualiserMsg cmd, true
        | ExXAxisIndex (index) -> 
            let model,cmd = Visualiser.update(Visualiser.XAxisIndexMsg <| index) (Map.find visId currentModel.VisModelMap)
            let nextMap = Map.add visId model currentModel.VisModelMap
            let nextModel = {currentModel with VisModelMap = nextMap}
            nextModel, Cmd.map VisualiserMsg cmd, true
        | ExTransformationChosen (transformationName) -> 
            let visModel,cmd = Visualiser.update(transformationName  |> Visualiser.TransformationChosenMsg ) (Map.find visId currentModel.VisModelMap)
            let nextMap = Map.add visId visModel currentModel.VisModelMap
            let nextModel = {currentModel with VisModelMap = nextMap}
            let nextModel = updateToolBoxVisualiserModel (ToolBoxVisualiserMsg.UpdateVisModelMsg visModel) nextModel
            let loadDataCmd =
                match visModel.Dataset with 
                | None -> Cmd.map VisualiserMsg cmd
                | Some (collector, dataset) -> asyncCmd requestData (collector, dataset, visModel.transformationList, visId) DataLoaded
            nextModel, loadDataCmd, true

        | ExTransformationOverwrite (transformationName, index) -> 
            let visModel,cmd = Visualiser.update((transformationName, index)  |> Visualiser.TransformationOverwriteMsg ) (Map.find visId currentModel.VisModelMap)
            let nextMap = Map.add visId visModel currentModel.VisModelMap
            let nextModel = {currentModel with VisModelMap = nextMap}
            let nextModel = updateToolBoxVisualiserModel (ToolBoxVisualiserMsg.UpdateVisModelMsg visModel) nextModel
            nextModel, Cmd.map VisualiserMsg cmd, true
        | ExTransformationRemoved (index) ->
            let visModel,cmd = Visualiser.update(index |> Visualiser.TransformationRemovedMsg ) (Map.find visId currentModel.VisModelMap)
            let nextMap = Map.add visId visModel currentModel.VisModelMap
            let nextModel = {currentModel with VisModelMap = nextMap}
            let loadDataCmd =
                match visModel.Dataset with 
                | None -> Cmd.map VisualiserMsg cmd
                | Some (collector, dataset) -> asyncCmd requestData (collector, dataset, visModel.transformationList, visId) DataLoaded
            nextModel, loadDataCmd, true
        | ExAllTransformationsRemoved ->
            let model, cmd = Visualiser.update(Visualiser.AllTransformationsRemovedMsg) (Map.find visId currentModel.VisModelMap)
            let nextMap = Map.add visId model currentModel.VisModelMap
            let nextModel = {currentModel with VisModelMap = nextMap}
            let loadDataCmd =
                match model.Dataset with 
                | None -> Cmd.map VisualiserMsg cmd
                | Some (collector, dataset) -> asyncCmd requestData (collector, dataset, model.transformationList, visId) DataLoaded
            nextModel, loadDataCmd, true
        | ExOpenToolBox -> 
            let nextModel = {currentModel with ToolboxOverlay = true}
            nextModel, Cmd.none, false
        | Visualiser.ExOpenFullScreen -> 
            let nextModel = {currentModel with FullScreenId = if currentModel.FullScreenId = -1 then visId else -1}
            nextModel, Cmd.none, false
    match model.ToolboxOverlay with
    | false -> model, cmd, shouldSave
    | true ->
        let visModel = ( model.VisModelMap |> Map.find visId)
        let toolboxModel, cmd' = model.ToolboxModel |> Toolbox.update (UpdateVisModelMsg visModel)
        let nextModel = {model with ToolboxModel = toolboxModel}
        nextModel, Cmd.batch [cmd; Cmd.map ToolBoxMsg cmd'], shouldSave
 
let HandleExToolBoxMsg msg currentModel = 
    match msg with
    | ExCloseToolBoxMsg  -> 
        let nextModel = {currentModel with ToolboxOverlay = false
                                           ToolboxModel = {currentModel.ToolboxModel with 
                                                            ToolboxVisualiserModel = {currentModel.ToolboxModel.ToolboxVisualiserModel with 
                                                                                        VisModel = {currentModel.ToolboxModel.ToolboxVisualiserModel.VisModel with
                                                                                                        ToolboxMode = true}}}}
        nextModel, Cmd.none, false

                       
let HandleUserDataLoadedMsg result currentModel = 
    match result with
    | Ok(json) ->
        let visModel = Serializer.getDecodedModel json
        let nextModel = { currentModel with VisModelMap = visModel}
        let rec loadData result (lst : VisModel list) =
            lst
            |> List.filter(fun x -> x.Dataset.IsSome)
            |> List.map (fun x -> asyncCmd requestData ((fst x.Dataset.Value), (snd x.Dataset.Value), x.transformationList, x.Id) LoadDataFromDatabase)
        
        let cmds = visModel |> Map.toList |> List.map(fun (k,v) -> v) |> loadData []
        nextModel, Cmd.batch cmds, false
    | Error(e) ->
        eprintf "%A" e
        currentModel, Cmd.none, false

let HandleDataLoaded result currentModel = 
    match result with
            | Ok(data, id, dataset) -> 
                let visModel = Map.find id currentModel.VisModelMap
                let visModel,visCmd = Visualiser.update(Visualiser.DataMsg <| (Some(dataset), Some(data))) visModel
                let nextMap = Map.add id visModel currentModel.VisModelMap
                let nextModel = {currentModel with VisModelMap = nextMap}
                let nextModel = updateToolBoxVisualiserModel (ToolBoxVisualiserMsg.UpdateVisModelMsg visModel) nextModel
                nextModel, Cmd.map VisualiserMsg visCmd, false
            | Error(e) ->
                eprintf "%A" e
                currentModel, Cmd.none, false
                
let HandleRequestDatasetMap result currentModel = 
    match result with
            | Ok(data) ->
                debugPrint "Requested datasetMap: %A" data
                currentModel, asyncCmd getDatasetMap () DatasetMapLoaded, false
            | Error(e) ->
                eprintf "%A" e
                currentModel, Cmd.none, false

let HandleDatasetMapLoaded result currentModel = 
    match result with
            | Ok(data) ->
                debugPrint "Load datasetMap %A" data
                let model,cmd = currentModel.ToolboxModel.DataCollectorsMenuModel |> CollectorMenu.update (MenuMsg.MenuDataMsg <| Some(data))
                let modelToolBox, cmd' =  currentModel.ToolboxModel |> Toolbox.update (ToolBoxMsg.LoadMenuDataMsg <| model)
                let nextModel = {currentModel with ToolboxModel = modelToolBox }
                nextModel, Cmd.none, false
            | Error(e) ->
                eprintf "%A" e
                currentModel, Cmd.none, false

let HandleAllTransformationsLoaded result currentModel = 
    match result with
            | Ok(transformationList) ->
                let newMap = Map.fold(fun s k (v:VisModel) -> 
                                        Map.add k ({ v with transformationList = transformationList}) s) Map.empty currentModel.VisModelMap
                let nextModel = { currentModel with VisModelMap = newMap }

                //change transformationMenu
                let newTransformationMenuModel = { nextModel.ToolboxModel.TransformationMenuModel with Datasets = (Map.add "Transformations" (transformationList |> List.toArray) Map.empty) |> Some}
                let modelToolBox, cmd' =  currentModel.ToolboxModel |> Toolbox.update (ToolBoxMsg.LoadMenuTransformationMsg <| newTransformationMenuModel)
                let nextModel = {currentModel with ToolboxModel = modelToolBox }


                nextModel, Cmd.none, false
            | Error(e) ->
                eprintf "%A" e
                currentModel, Cmd.none, false

let HandleTransformationLoaded result currentModel = 
    match result with
    | Ok(transformation) ->
        let nextModel = debugPrint "loaded transformation: %A" transformation; updateToolBoxVisualiserModel (SetLoadedTransformationBody transformation) currentModel
        nextModel, (asyncCmd validateTransformationBody (transformation) TransformationValidated), false 
    | Error(e) ->
        eprintf "%A" e
        currentModel, Cmd.none, false

let HandleTransformationSaved result visId currentModel= 
    match result with
    | Ok(_) -> debugPrint "Saved transformation%s" "" 
               let loadDataCmd =
                   let visModel = (Map.find visId currentModel.VisModelMap)
                   match visModel.Dataset with 
                   | None -> Cmd.none
                   | Some (collector, dataset) -> asyncCmd requestData (collector, dataset, visModel.transformationList, visId) DataLoaded
               currentModel, Cmd.batch [asyncCmd requestTransformations () InitialTransLoaded; loadDataCmd], true 
    | Error(e) ->
        eprintf "%A" e
        currentModel, Cmd.none, true

let HandleTransformationValidated result currentModel= 
    match result with
    | Ok(errors) ->
        let nextModel =
            currentModel 
            |> updateToolBoxVisualiserModel (SetTransformationValidateWith errors)
        nextModel, Cmd.none, false
    | Error(e) ->
        eprintf "%A" e
        currentModel, Cmd.none, false

let HandleExToolBoxVisualiseMsg msg (currentModel :Model) =
    let model, cmd =
        match msg with
        | ExSetIsShowingTransformationModal (transformationName, index) ->
            let nextModel = updateToolBoxVisualiserModel (SetIsShowingTransformation (true, transformationName+".1", index)) currentModel
            nextModel, (asyncCmd requestTransformationText (transformationName) TransformationLoaded)
        | ExRequestClosingTransformationModal -> 
            let nextModel = updateToolBoxVisualiserModel (SetWantToCloseTransformation) currentModel
            nextModel, Cmd.none
         | ExSetTransformationName value ->
             let nextModel = updateToolBoxVisualiserModel (SetTransformationName value) currentModel
             nextModel, Cmd.none
        | ExSetTransformationBody body ->
            let nextModel = updateToolBoxVisualiserModel (SetTransformationBody body) currentModel
            nextModel, (asyncCmd validateTransformationBody (body) TransformationValidated)
        | ExCloseAndDiscard -> 
            let nextModel = updateToolBoxVisualiserModel (SetCloseTransformationOverlay) currentModel
            nextModel, Cmd.none
        | ExCloseAndSave (transformationName, transformationBody, visId) ->
            let createMsg visId result =
                TransformationSaved (result, visId)
            let nextModel =
                currentModel
                |> updateToolBoxVisualiserModel (SetIsShowingTransformation (false, "", -1))
                |> updateToolBoxVisualiserModel (SetCloseTransformationOverlay)
            nextModel, (asyncCmd requestWriteTransformation (transformationName, transformationBody) (createMsg visId))
        | ExCloseAcceptModal -> 
            let nextModel = updateToolBoxVisualiserModel (SetCloseAcceptModal) currentModel
            nextModel, Cmd.none
        | ExDisableDragging -> 
            updateToolBoxVisualiserModel (DisableDragging) currentModel, Cmd.none
    model, cmd, false
        
let HandleMenuMsg msg (currentModel : Model) = 
    match msg with
    | ExActiveMenuMsg(collector, menuType) ->
        let cModel, cCmd = CollectorMenu.update (UpdateActiveMenuMsg (if menuType = "Transformations" then "" else collector))
                            <| currentModel.ToolboxModel.DataCollectorsMenuModel       
        let tModel, tCmd = CollectorMenu.update (UpdateActiveMenuMsg (if menuType = "Transformations" then collector else ""))
                            <| currentModel.ToolboxModel.TransformationMenuModel
        let nextToolboxModel = { currentModel.ToolboxModel with DataCollectorsMenuModel = cModel
                                                                TransformationMenuModel = tModel}
        let nextModel = { currentModel with ToolboxModel = nextToolboxModel}
        nextModel, Cmd.batch[Cmd.map CollectorMsg cCmd
                             Cmd.map CollectorMsg tCmd], false
    | ExSetDragging dragName -> 
        updateToolBoxVisualiserModel (SetDragging dragName) currentModel , Cmd.none, false


let update (msg : Msg) (currentModel : Model) : Model * Cmd<Msg> =  
    let model, cmd, shouldSave = 
        currentModel |>
        match msg with
        |  AddVisualiser                      -> HandleAddVisualiser
        |  ClientMsg msg                      -> HandleClientMsg msg
        |  RemoveAll                          -> HandleRemoveAll
        |  SaveDataMsg                        -> HandleSaveDataMsg
        |  DataSavedMsg(result)               -> HandleDataSavedMsg result
        |  LoadUserDataMsg                    -> HandleLoadUserDataMsg
        |  ExVisualiserMsg(msg)               -> HandleExVisualiserMsg msg
        |  ExToolBoxMsg(msg)                  -> HandleExToolBoxMsg msg
        |  UserDataLoadedMsg(result)          -> HandleUserDataLoadedMsg result
        |  DataLoaded(result)                 -> HandleDataLoaded result
        |  TransformationLoaded(result)       -> HandleTransformationLoaded result
        |  TransformationSaved(result, visId) -> HandleTransformationSaved result visId
        |  TransformationValidated(result)    -> HandleTransformationValidated result
        |  RequestDatasetMap(result)          -> HandleRequestDatasetMap result 
        |  DatasetMapLoaded(result)           -> HandleDatasetMapLoaded result 
        |  TransformationsLoaded(result)      -> HandleAllTransformationsLoaded result    
        |  InitialTransLoaded(result)         -> HandleAllTransformationsLoaded result
        |  LoadDataFromDatabase(result)       -> HandleDataLoaded result
        |  ExToolBoxVisualiseMsg(msg)         -> HandleExToolBoxVisualiseMsg msg
        |  ExCollectorMsg(msg)                -> HandleMenuMsg msg
        |  m                                  -> 
                                                 debugPrint "Message (%A) not handled here" m
                                                 (fun _ -> currentModel, Cmd.none, false)
    debugPrint "Message: %s" (msg.ToString ())                                         
    match shouldSave with
    | false -> model, cmd
    | _ ->
        let newModel, saveCmd = saveData model
        newModel, Cmd.batch[saveCmd; cmd]
let margin t r b l =
    Chart.Margin { top = t; bottom = b; right = r; left = l }
//Create an HTML element based on an AST node

let createContainer customClass columns =
    let columns = 
            columns
            |> List.map(fun column ->
            Column.column [ Column.Width (Screen.All, column.Width) ]
                (column.Content)
            ) |> Columns.columns [ ]
    Container.container [ 
                            if customClass |> Option.isSome then yield Container.Option.CustomClass(customClass.Value)
                        ] 
                        [ columns ]

let createVisualisers model dispatch = 
    div [  ] [
        Button.button [ Button.OnClick(fun _ -> dispatch AddVisualiser); Button.Size ISize.IsSmall; Button.Color IColor.IsWhite; Button.CustomClass "floatRight"] 
            [
            Icon.icon [][ Fa.i [ Fa.Solid.Plus ][]]
            ]
        model.VisModelMap
        |> Map.toList
        |> makeTiles dispatch
        |> Tile.ancestor [Tile.Size Tile.Is12; Tile.IsVertical] 
    ]
    

let createVisualisersContainer model dispatch = 
    [{ Width = Column.ISize.Is12; Content = [createVisualisers model dispatch]}]
    |> createContainer None

let createNavbarItem model dispatch navbarMenuItem = 
    match navbarMenuItem with
       | Single s -> Navbar.Item.a [ ] [ str s]
       | _ -> failwith "Don't yet know how to create submenus"
let createNavBar model dispatch = 
    let nb =
        {
            Menu  =[] 
            Color = IColor.IsWhite
            Brand = "Flowerpot"
        }
        
    Navbar.navbar [ nb.Color |> Navbar.Color ]
        [ Container.container [ ]
            [ 
                Navbar.Brand.div [ ][ 
                    Navbar.Item.a [ Navbar.Item.CustomClass "brand-text" ] [ nb.Brand |> str ]
                ]
                Navbar.menu [ ] [ Navbar.Start.div [ ]
                    (nb.Menu |> List.map (createNavbarItem model dispatch)) ]
            ]
        ]
    
let createDefaultWindow (dispatch : Msg -> unit) (model:Model) =
    let navBar = createNavBar model dispatch

    let pageContent =
        [
            { Width = Column.ISize.Is12; Content = [createVisualisersContainer model dispatch]}
        ]
        |> createContainer None

    [{ Width = Column.ISize.Is12; Content = [navBar; pageContent]}]
    |> createContainer (Some "page-container")

let createToolBox (dispatch : Msg -> unit) (model:Model) = 
    Toolbox.view (model.ToolboxModel) (ExToolBoxMsg >> dispatch) ( ExCollectorMsg >> dispatch) (ExToolBoxVisualiseMsg >> dispatch) (ExVisualiserMsg >> dispatch)


let show (dispatch : Msg -> unit) model =
    let content =
        match model.ToolboxModel.DataCollectorsMenuModel.Datasets with
        | None -> str "Loading data"
        | _ ->
            match model.ToolboxOverlay with
            | false    ->  createDefaultWindow dispatch model
            | true ->  createToolBox dispatch model
    let sendOpenFullScreen id =  (fun _ -> id |> ExOpenFullScreen |> (ClientMsg >> dispatch))
    let modal =
        if model.FullScreenId = -1 then str ""
        else
            let visModel = (model.VisModelMap |> Map.find model.FullScreenId ) 
            let visModel = {visModel with Size = Some (800., 500.)}
            basicModal true (sendOpenFullScreen -1 ) (Visualiser.view visModel (ExVisualiserMsg >> dispatch))
    div [][
        content
        modal
    ]
        
//The above code is at present not used but are examples taken from the SAFE bulma admin example
let view model (dispatch : Msg -> unit) =
    show dispatch model


#if DEBUG
open Elmish.Debug
open Elmish.HMR
#endif

Program.mkProgram init update view
#if DEBUG
|> Program.withConsoleTrace
//|> Program.withHMR
#endif
|> Program.withReactBatched "elmish-app"
#if DEBUG
|> Program.withDebugger
#endif
|> Program.run


(*
//Creates an HTML specifically for the Breadcrumb node types
and private createBreadcrumbs dispatch model bcs =
    bcs
    |> List. map(fun bc ->
        Breadcrumb.item [ ]
          [ a [ ] [ bc |> createElement dispatch model] ])
    |> Breadcrumb.breadcrumb [ ]
*)