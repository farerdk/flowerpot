﻿module Toolbox
open Elmish
open Elmish.React
open Fable.React
open Fable.React.Props
open Shared.Flowerpot
open Shared.Hobbes
open Fulma
open CollectorMenu
open Shared.Hobbes
open ToolboxVisualiser
open Fable.Recharts
module P = Fable.React.Props
open Fable.Recharts.Props
open Fable.Core.JS
open Fable.FontAwesome
open System
open Visualiser

type ToolboxModel = {
        VisualiserId : int
        DataCollectorsMenuModel : MenuModel
        TransformationMenuModel : MenuModel
        ToolboxVisualiserModel : ToolboxVisualiserModel
    }


type ExToolBoxMsg =
| ExCloseToolBoxMsg

type ToolBoxMsg =
| LoadMenuDataMsg of MenuModel
| LoadMenuTransformationMsg of MenuModel
| UpdateVisModelMsg of VisModel

    
let init id : ToolboxModel= {
                                VisualiserId = -1
                                DataCollectorsMenuModel = CollectorMenu.init "DataCollector"
                                TransformationMenuModel = CollectorMenu.init "Transformations"
                                ToolboxVisualiserModel =  (Visualiser.init -1) |> ToolboxVisualiser.init}
        
let update (msg) (currentModel : ToolboxModel) : ToolboxModel * Cmd<ToolBoxMsg> = 
    let model =
        match msg with
        | LoadMenuDataMsg menuModel -> {currentModel with DataCollectorsMenuModel = menuModel}
        | LoadMenuTransformationMsg menuModel -> {currentModel with TransformationMenuModel = menuModel}
        | UpdateVisModelMsg visModel ->
            let toolboxVisualiserModel, _ = currentModel.ToolboxVisualiserModel |> ToolboxVisualiser.update (ToolboxVisualiser.UpdateVisModelMsg visModel)
            {currentModel with ToolboxVisualiserModel = toolboxVisualiserModel}
    model, Cmd.none



let view (model : ToolboxModel) (dispatch : ExToolBoxMsg -> unit) dispatchMenu dispatchToolboxVisualiser dispatchVisualiser = 
    printfn "Create ToolBox %A" model.ToolboxVisualiserModel.VisModel.transformationList
    Columns.columns [ ] [
        Column.column [ Column.CustomClass "ScrollMenu"; Column.Width (Screen.All, Column.Is3) ] 
            [
                CollectorMenu.view (model.DataCollectorsMenuModel) dispatchMenu
                CollectorMenu.view (model.TransformationMenuModel) dispatchMenu
            ]
        Column.column [ Column.Width (Screen.All,  Column.Is9) ] 
            [
            str "Toolbox mode"
            Button.button [Button.CustomClass "CloseToolboxButton"; Button.OnClick(fun _ -> dispatch ExCloseToolBoxMsg); Button.Size ISize.IsSmall; Button.Color IColor.IsWhite; Button.CustomClass "floatRight"] 
                            [Icon.icon [][ Fa.i [ Fa.Solid.WindowClose ][]] ]
            ToolboxVisualiser.view (model.ToolboxVisualiserModel) dispatchToolboxVisualiser dispatchVisualiser 
                          ]
        ]
       
    