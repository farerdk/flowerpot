﻿module Visualiser

open Browser
open Elmish
open Fable.React
open Fable.React.Props
open Shared.Hobbes
open Fulma
open Fable.Recharts
module P = Fable.React.Props
open Fable.Recharts.Props
open Fable.FontAwesome
open System
open Fable.Core
open Flowerpot
open Fable.React.ReactiveComponents
open System.Collections.Generic

let debugSetting = false
let debug = debugSetting && (window.location.hostname = "localhost")
let debugPrint t s =
    if debug then printf t s else ()
type VisualiserType = 
   Table
   | Chart2D
   | Pie
   with 
       override x.ToString() = 
          match x with
          | Table -> "table"
          | Chart2D -> "chart2d"
          | Pie -> "pie"
       static member Parse (s:string) = 
          match s with
          "table" -> Table
          | "chart2d" -> Chart2D
          | "pie" -> Pie 
          | _ -> failwithf "Don't knowhow to parse %s as a VisualiserType" s
            
type Stack = {
    Stacks : Map<string, int>
    Stacking : string * bool
    StackId : int
    ColorMap : Map<string, string>
}

type VisModel = {  
    VisualiserType: VisualiserType
    Dataset : (string * string) option
    DataSeries : ColumnSerie [] option
    Rows : obj [] []
    DataObject : obj []
    Id : int
    xAxisIndex: int
    transformationList : string list
    ToolboxMode : bool
    Size : (float * float) Option
    IsStacking : int option
}

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
module VisModel = 
    let filledColumns (v : VisModel) =
        match v.DataSeries with
        None -> [||]
        | Some series ->
            series
            |> Array.collect(ColumnSerie.filledColumns)
    let filledDataSeries v =
        let series = 
            v.DataSeries
            |> Option.bind(
                Array.map(fun serie ->
                    {serie with 
                        DataColumns = 
                            serie.DataColumns
                            |> Array.filter(fun dc -> dc.Values |> List.isEmpty |> not)
                    }
                ) 
                >> Array.filter(fun serie -> serie.DataColumns |> Array.isEmpty |> not)  
                >> Some
            )       
        match series with
        None -> [||]
        | Some s -> s
    let columns (v : VisModel) =
        match v.DataSeries with
        None -> [||]
        | Some series ->
            series
            |> Array.collect(ColumnSerie.columns)

    let columnNames v = 
        v
        |> columns
        |> Array.map(fun c -> c.Name)

    let numericColumns v =
        match v.DataSeries with
        None -> [||]
        | Some series ->
            series
            |> Array.filter(ColumnSerie.isNumeric)
            |> Array.collect(ColumnSerie.filledColumns)

    let tryFindColumn name (v :VisModel) =
        v
        |> columns
        |> Array.tryFind (fun c -> c.Name = name)

    let tryFindSeriesByColumnName name (v : VisModel) = 
        v.DataSeries
        |> Option.bind(
            Array.tryFind(fun serie ->
                serie.DataColumns
                |> Array.tryFind(fun c -> c.Name = name)
                |> Option.isSome
            )
        )

    let appendDataSerie serie model = 
        {
            model with 
                DataSeries = 
                    match model.DataSeries with
                      None -> [| serie |]
                      | Some series -> [|serie|] |> Array.append series
                    |> Some
        }

    let updateSerieColumn column model = 
        {
            model with
                DataSeries = 
                    match model.DataSeries with
                    None -> failwith "Data series is None. Can't update None"
                    | Some series ->
                        series
                        |> Array.map(fun serie ->
                            if serie 
                               |> ColumnSerie.tryFindColumn column.Name 
                               |> Option.isNone then
                               serie
                            else
                                serie
                                |> ColumnSerie.updateColumn column
                        ) |> Some
        }

    let setChartTypeOfSerieByColumnName columnName chartType v = 
        let dataSeries = 
            v.DataSeries
            |> Option.bind(fun dataSeries ->
                dataSeries
                |> Array.map(fun serie ->
                     match serie |> ColumnSerie.tryFindColumn columnName with
                     None -> 
                         serie
                     | Some _ ->
                         printfn "Found column. Changing %s to %s" (string serie.ChartType) (string chartType)
                         {
                             serie with
                                 ChartType = chartType
                         }                                        
                ) |> Some
            )
        if dataSeries |> Option.bind (Array.toList >> Some) = (v.DataSeries |> Option.bind (Array.toList >> Some)) then
           printfn "No update happened %A. Name %s" dataSeries columnName
        {
             v with
                 DataSeries = dataSeries
        }

    [<Emit("""$0 && $0.length > 0 ? $0[0].map((col, i) => $0.map(row => row[i])) : $0""")>]
    let private transpose (_ : 'a [] []) : 'a [] [] = Util.jsNative

    let setColumnData cols model = 
        
        let newModel =
            cols
            |> Array.fold(fun tempModel column ->
                    match tempModel
                          |> tryFindSeriesByColumnName column.Name with
                    None ->
                        tempModel
                        |> appendDataSerie
                            {
                                HasTrendline  = false
                                DataColumns = [| column |]
                                ChartType = Line
                                ToolTip = None
                            } 
                    | Some _ ->
                        tempModel
                        |> updateSerieColumn column
            ) model

        let rawData = 
            newModel
            |> filledColumns
            |> Array.map (fun dc -> 
                let mapper = 
                    match dc.Type with
                    Number -> float >> box
                    | String -> fun v -> v :> obj
                    | DateTimeOffset -> DateTimeOffset.Parse >> box
                try
                    dc.Values
                    |> Array.ofList
                    |> Array.map(fun value -> 
                        try
                            (dc.Name, mapper value) 
                        with e ->
                           eprintf "Error when mapping. Message: %s data column: %s" e.Message dc.Name
                           reraise()
                        
                    )
                with e ->
                   eprintf "Error message: %s data column: %s" e.Message dc.Name
                   reraise()
            )
            |> transpose

        let dataObject =  
            rawData
            |> Array.map(Fable.Core.JsInterop.createObj)

        let rows =
           rawData
           |> Array.map(Array.map(snd))

        {
            newModel with 
                    Rows = rows
                    DataObject = dataObject
        }


type ExVisMsgContent =
| ExRequestDataset of string * string * (string list)
| ExVisualiserType of VisualiserType
| ExDeleteMsg
| ExOpenToolBox
| ExOpenFullScreen
| ExGraphChange of nameOfSerie:string * ChartType
| ExXAxisIndex of int
| ExTransformationChosen of  string
| ExTransformationOverwrite of  string*int
| ExTransformationRemoved of int
| ExAllTransformationsRemoved
| ExStacking of int
| ExStopStacking
| ExDisbandColumnSerie of int
| ExAddToStack of int
| ExStacks of Map<string, int>

type ExVisMsg = ExVisMsgContent * int

type VisMsg =
| DataMsg of (string*string) option *(DataColumn[] option)
| DataSetMsg of (string*string)
| TypeMsg of VisualiserType
| GraphChangeMsg of nameOfSerie:string * ChartType
| XAxisIndexMsg of  int
| TransformationChosenMsg of  string
| TransformationOverwriteMsg of string*int
| TransformationRemovedMsg of int
| AllTransformationsRemovedMsg
| StartStacking of int
| StopStacking
| StacksMsg of Map<string, int>
| Save


type Table =
    {
        thead : string list
        tbody : obj [] []
    }
let colorArray = [|"#e6194B"; "#3cb44b"; "#ffe119"; "#4363d8"; "#f58231"; "#911eb4"; "#42d4f4"; "#f032e6"; "#bfef45"; "#469990"; 
                       "#9A6324"; "#fffac8"; "#800000"; "#aaffc3"; "#808000"; "#ffd8b1"; "#000075"|]
let initialStack = {
        Stacks = Map.empty
        Stacking = ("",false)
        StackId = 0
        ColorMap = Map.empty
    }


let init id : VisModel=
    {
        VisualiserType      = Table
        Dataset             = None
        DataSeries          = None
        Rows                = [||]
        DataObject          = [||]
        Id                  = id
        xAxisIndex          = 0
        transformationList  = []
        ToolboxMode         = false
        Size                = None
        IsStacking = None
    }
let dataColumnIsNumber dc =
    match dc.Type with
    | Number -> true
    |_       -> false

let rnd = System.Random()
let randomColor() =  
    let list = ["1";"2";"3";"4";"5";"6";"7";"8";"9";"A";"B";"C";"D";"E";"F";]
    [for _ in 1..6 ->
        list.[rnd.Next(List.length list)]
    ] |> System.String.Concat
    |> sprintf "#%s"
       
let combineToTuple secondPart first = first, secondPart
    
let update (msg) (currentModel : VisModel) : VisModel * Cmd<VisMsg>=
    
    let createColorMap value =
         value
         |> Array.filter dataColumnIsNumber 
         |> Array.indexed 
         |> Array.fold(fun s (index, data) -> s |> Map.add data.Name (colorArray.[index % colorArray.Length] ) ) Map.empty

    let resetStack value = 
        let stackMap =
            value
            |> Array.indexed 
            |> Array.fold(fun s (index, data) -> s |> Map.add data.Name index ) Map.empty
    
        {initialStack with ColorMap = createColorMap value
                                      Stacks = stackMap
                                      StackId = ((Array.length value)+2)}
    let keyInMap map key = match map |> Map.tryFind key with | None -> false | Some _ -> true
    let dcValueCheck (data: DataColumn [] Option) f = 
        data.Value 
        |> Array.exists (fun dc -> not (f dc.Name)) |> not
    
    match msg with
    | DataMsg(dataset, dataColumns) ->
        let nextModel = 
            match dataColumns with
            None -> currentModel
            | Some dataColumns ->
                currentModel
                |> VisModel.setColumnData dataColumns
        { nextModel with Dataset = dataset}, Cmd.none
    | DataSetMsg (dataset) ->
        let nextModel = { currentModel with Dataset = Some dataset}
        nextModel, Cmd.none
    | TypeMsg(_type) ->
        let nextModel = { currentModel with VisualiserType = _type}
        nextModel, Cmd.none
    | XAxisIndexMsg (index) ->
        //TODO: XAxisIndex might not work with the ColumnSeries approach
        //maybe drag and drop of columns in table view should be used instead
        //then drag and drop could also be used for stacking/grouping
        let nextModel = 
            { 
                currentModel with 
                               xAxisIndex = index 
                               DataSeries = 
                                   currentModel.DataSeries
                                   |> Option.bind(fun dataSeries ->
                                       dataSeries
                                       |> Array.collect(fun serie ->
                                           serie.DataColumns
                                           |> Array.map(fun dc ->
                                               {
                                                   serie with DataColumns = [|dc|]
                                               }
                                           )
                                       ) |> Some
                                   )
            }
        nextModel, Cmd.none
    | TransformationChosenMsg transformationName ->
        let nextModel = { currentModel with transformationList = currentModel.transformationList @ [transformationName]}
        nextModel, Cmd.none
    | TransformationOverwriteMsg (transformationName, index) ->
        let transformationList =
            currentModel.transformationList
            |> List.indexed
            |> List.map (fun (_index, _name) -> if _index = index then transformationName else _name)
        let nextModel = { currentModel with transformationList = transformationList}
        nextModel, Cmd.none
    | TransformationRemovedMsg index -> 
        let indexedTransList = List.indexed currentModel.transformationList
        let nextModel = { currentModel with transformationList = List.filter (fun (i, _) -> index <> i) indexedTransList |> List.map (fun (_, name) -> name)}
        nextModel, Cmd.none
    | AllTransformationsRemovedMsg ->
        let nextModel = {currentModel with transformationList = []}
        nextModel, Cmd.none
    | StartStacking(index) ->
        { currentModel with
                         IsStacking = Some index}, Cmd.none
    | StacksMsg(stack) ->
        eprintf "changing staks not implemented"
        currentModel,Cmd.none
    | GraphChangeMsg (name, chartType) ->
        currentModel
        |> VisModel.setChartTypeOfSerieByColumnName name chartType, Cmd.ofMsg Save 

let margin t r b l =
    Chart.Margin { top = t; bottom = b; right = r; left = l  }

let getWhereFstEqual nameToMatch lst=
  lst
  |> Array.tryFind(fun (name, _, t) ->
      name = nameToMatch && t = Number
  ) |> Option.bind(fun (_,value,_) ->
       let ok, v = System.Double.TryParse value
       if ok then Some v
       else Some (0.0)
  ) |> Option.get
   

let getMapValue map key =
    match Map.tryFind key map with 
    | None -> 0, None
    | Some (v, toolTipOption) -> (Int32.Parse v, toolTipOption)


let listOfColors = Seq.toList (seq { for n in 1 .. 100 do yield randomColor() })




let mapGraphType isStacking id dispatch index columnSerie = 
        let color =
            match isStacking with
            Some i when i = index ->
                 "#000000"
            | _ -> 
                colorArray.[index % colorArray.Length]
        let handleStacking _ =
            match isStacking with
            | Some (stackIndex) -> 
                       debugPrint "stack index = %d" stackIndex
                       if stackIndex = index then
                           ExDisbandColumnSerie(index) |> combineToTuple id |> dispatch
                           ExStopStacking |> combineToTuple id |> dispatch
                       else
                           ExAddToStack(index)  |> combineToTuple id |> dispatch
                            
            | None -> ExStacking(index) |> combineToTuple id |> dispatch
        
        match columnSerie.ChartType with
        | TrendLine when columnSerie.ToolTip.IsSome ->
            fun (name : string) ->
                line [   
                    Cartesian.Name columnSerie.ToolTip.Value
                    //Cartesian.Label false
                    Cartesian.Type Monotone
                    Cartesian.ActiveDot false
                    Cartesian.DataKey name
                    P.Stroke "#808080"
                    P.StrokeWidth 2.
                    P.StrokeDasharray "5 5"
                ] []  
        | Line  ->
            fun name ->
                line [
                     //Cartesian.OnClick (fun _ -> printfn "Pressed %s" name; ((model.GraphTypes |> Map.add name 1), model.Id) |> ExGraphChange |> dispatch)    
                     Cartesian.Type Monotone
                     Cartesian.DataKey name
                     Cartesian.OnClick handleStacking
                     Cartesian.StackId index
                     P.Stroke color
                     P.StrokeWidth 4.
                ] []   
        | Bar -> 
            fun name -> 
                bar [
                     Cartesian.DataKey name
                     Cartesian.OnClick handleStacking
                     Cartesian.StackId index
                     P.Fill color
                ] []
        | Area -> 
            fun name ->
                area [
                     Cartesian.Type Monotone
                     Cartesian.DataKey name
                     Cartesian.OnClick handleStacking
                     Cartesian.StackId index
                     P.Stroke color
                     P.Fill color
                ] []
        | ScatterPlot -> 
            fun name ->
                scatter [
                     Cartesian.Type Monotone
                     Cartesian.DataKey name
                     Cartesian.OnClick handleStacking
                     Cartesian.StackId index
                     P.Stroke color
                     P.Fill color] []

let changeGraph (model : VisModel) name =
    match model
          |> VisModel.tryFindSeriesByColumnName name with
    None ->
       failwith "Trying to change the chart type of none existing column"
    | Some serie ->
        match serie.ChartType with
        | ChartType.Area -> (Fa.Solid.ChartArea, (ExGraphChange(name,ChartType.Line), model.Id))
        | ChartType.Line -> (Fa.Solid.ChartLine, (ExGraphChange (name, ChartType.Bar), model.Id))
        | ChartType.Bar -> (Fa.Solid.ChartBar, (ExGraphChange (name, ChartType.Area), model.Id))

let createButton dispatch (iconType, msg) =
    Button.button 
        [
            Button.OnClick((fun _ -> msg |> dispatch))
            Button.Size ISize.IsSmall
            Button.Color IColor.IsWhite 
        ]
        [
            Icon.icon 
                [] 
                [ Fa.i 
                    [ iconType ][ ] 
                ]
        ]

let createDropDownTable id items dispatch dispatchType : ReactElement list =
    items 
    |> List.map(fun (icon, msg) ->   
            Dropdown.Item.a
                [ Dropdown.Item.Props [OnClick (fun _ -> msg |> dispatch)]] 
                [Icon.icon [] [ Fa.i [ icon ][ ] ]]
    )
let createDropDown model dispatch iconType lst: ReactElement =
    Dropdown.dropdown [ Dropdown.IsHoverable ]
        [ div [ ][
            Button.button [Button.Size ISize.IsSmall]
                [ span [ ]
                    [
                    Icon.icon [] [ Fa.i [ iconType ][ ] ]
                    Icon.icon [ Icon.Size IsSmall ] [ Fa.i [ Fa.Solid.AngleDown ] [ ] ] ]
                ]
            Dropdown.menu [ ] [
                Dropdown.content [ ] [ 
                    for item in (createDropDownTable model.Id lst dispatch ExTransformationChosen) do yield item
                    ] ] ]
        ]

let getIcon name = 
    match name with
    | Table    -> Fa.Solid.Table
    | Chart2D    -> Fa.Solid.ChartLine
    | Pie -> Fa.Solid.ChartPie

let createMenu model dispatch = 
      let createItemTuple name = (getIcon name, (ExVisualiserType (name), model.Id))
      let icon = getIcon model.VisualiserType
      let leftButtons = seq{
                            yield [
                                createItemTuple Table
                                createItemTuple  Chart2D
                                createItemTuple Pie
                            ] |> createDropDown model dispatch icon 
                        }
      let rightButtons =  
        if model.ToolboxMode then
            Seq.empty
        else 
            [(Fa.Solid.Wrench, (ExOpenToolBox, model.Id))
             (Fa.Solid.ExpandArrowsAlt, (ExOpenFullScreen, model.Id))
             (Fa.Solid.WindowClose, (ExDeleteMsg, model.Id))]
             |> List.map (createButton dispatch)
             |> List.toSeq
        
      div []
        [
            leftButtons  |> span []
            rightButtons |> span [ Class "floatRight" ]
        ]

let createXAxisFiller length =
    [0 .. length - 1]
    |> List.map string

let ComposedChart dispatch model =
      try
          let rawData = 
              match model.DataSeries with
              None -> [||]
              | Some data -> data
          let usableXAxis = 
              rawData |> Array.filter (fun dc -> 
                  let serie = ColumnSerie.combinedSerie dc
                  serie.Values
                  |> List.length = 
                      (serie.Values
                       |> List.distinct
                       |> List.length)
              )

          let artificalXAxis = 
              if Array.length rawData = 1 || Array.isEmpty usableXAxis then 
                  [|{
                    DataColumns = 
                        [|{Name = "xAxis"; Values = createXAxisFiller (rawData |> Array.head |> ColumnSerie.length); Type = DataType.Number}|] 
                    HasTrendline = false
                    ToolTip = None
                    ChartType = ChartType.Line 
                  }|]
              else Array.empty
          let rawData = Array.append artificalXAxis rawData
          let usableXAxis = Array.append artificalXAxis usableXAxis

          let xAxisDc = usableXAxis.[model.xAxisIndex % usableXAxis.Length]
          let xAxisName = xAxisDc.DataColumns.[0].Name
          let isXAxisNumber = 
              xAxisDc 
              |> ColumnSerie.isNumeric
          
          let series = 
              model
              |> VisModel.filledDataSeries
              |> Array.indexed
              |> Array.collect(fun (index,serie) -> 
                  let formatter = mapGraphType model.IsStacking model.Id dispatch index serie
                  serie.DataColumns
                  |> Array.map(fun column -> 
                      column.Name
                      |> formatter
                  )
              ) |> List.ofArray
          
          let handleClick _ =  
              model.xAxisIndex + 1
              |> ExXAxisIndex
              |> combineToTuple model.Id
              |> dispatch
          let width, height = match model.Size with | None -> 400., 250. | Some size -> size
          Browser.Dom.console.log("Data object", model.DataObject)
          composedChart
            [ Class "chart"
              margin 0. 0. 0. 0.
              Chart.Width width
              Chart.Height height
              Chart.Data model.DataObject
              Chart.SyncId  "1"
            ]
            (series@
             [
                    cartesianGrid [
                        P.StrokeDasharray "1 1"
                    ] []
                    tooltip [] []
                    xaxis [
                      yield Cartesian.DataKey xAxisName
                      yield Cartesian.OnClick handleClick
                      if isXAxisNumber then yield Cartesian.Type Number
                      yield Cartesian.MinTickGap 0.5
                    ] []
                    legend [] []
                    yaxis [] []
             ])
      with e -> 
          eprintfn "ERROR: %s" (e.ToString())
          div [] [ e.ToString() |> str ]

let createPieChart dispatch (model: VisModel)  =  
    
    
    match model.DataSeries with
    None -> str "No data to draw"
    | Some columns ->
        let series = 
            columns
            |> Array.filter (ColumnSerie.isNumeric)
            |> Array.collect(ColumnSerie.columns)
        
        let cells = 
            [for i in 0..model.DataObject.Length - 1 -> 
                let color = colorArray.[i % colorArray.Length]
                Fable.Recharts.cell [Fable.Recharts.Props.Cell.Fill color] []
            ]
            
        let width, height = match model.Size with | None -> 400., 260. | Some size -> size
        //TODO: this should filter on the selected value (pie slice) instead of simply changing the serie
        //let onclickMethod = (fun _ ->(model.xAxisIndex + 1) |> ExXAxisIndex |> combineToTuple model.Id |> dispatch)
        let serieNames = 
            model
                |> VisModel.numericColumns
                |> Array.map(fun c -> c.Name)
        div[][
            div [ClassName "VisualiserTitle"] [ str (sprintf "%s" (model.Dataset.Value |> snd))  ]

            pieChart 
                [ 
                    Class "chart"
                    margin 0. 0. 0. 0.
                    Chart.Width width
                    Chart.Height height 
                ]
                (serieNames
                 |> List.ofArray
                 |> List.indexed
                 |> List.map(fun (i,name) ->
                    pie
                        [
                            yield Cartesian.DataKey name
                            yield Chart.Data model.DataObject
                            yield Chart.OuterRadius ((i + 2) * 30)
                            if i <> 0 then yield Chart.InnerRadius ((i + 2) * 30 - 20)
                            if i = (serieNames.Length - 1) then yield Cartesian.Label true
                        ] cells
                 ) |> List.append
                        [
                            tooltip[][]
                            label[][]
                        ])
        ]

let visualizeData (dispatch) (model : VisModel)  = 
    match model.VisualiserType  with
    | Table -> 
        if model.Rows |> isNull || model.Rows |> Array.isEmpty then
            str "no table data"
        else 
            let chartableColumns =
               model
               |> VisModel.numericColumns
               |> Array.map(fun c -> c.Name)
               |> Set.ofArray
            let headers = 
                model
                |> VisModel.filledColumns
                |> Array.map(fun c -> c.Name)
            let rows = model.Rows
            div [ClassName "tableDiv"] [
                Table.table [Table.IsStriped][
                    thead [ ] [
                        headers
                        |> Array.map(fun e ->
                            let isChartable = chartableColumns |> Set.contains e 
                            printfn "%s is chartable: %b" e isChartable
                            th [] [
                                    span [] [
                                              yield str e
                                              if isChartable then 
                                                  yield changeGraph model e |> createButton dispatch 
                                            ]
                                 ]
                        )
                        |> tr [ ] ]
                    rows
                    |> Array.take(rows |> Array.length |> min 100)
                    |> Array.map(fun row ->
                        row
                        |> Array.map(fun cellValue -> 
                            td [] [
                                  cellValue |> string |> str
                            ]
                        )
                        |> tr [ ]
                    )
                    |> tbody [ ]
                ]
            ]
    | Chart2D -> ComposedChart dispatch model
    | Pie -> createPieChart dispatch model 


let createVisTitle model =
    let title = 
        match model.Dataset with
        | None -> ""
        | Some (_,ds)  ->
            match model.transformationList with
            [] -> ds
            | l -> ds + " - " + (l |> List.last)
    div [ClassName "VisualiserTitle"]
        [
            str title       
        ]

let view (model : VisModel) (dispatch : ExVisMsg -> unit)  =
    div [][    
        createMenu model dispatch
        createVisTitle model
        visualizeData dispatch model
    ]