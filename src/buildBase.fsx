#r "paket: groupref build //"
#load "../.fake/build.fsx/intellisense.fsx"


#if !FAKE
#r "netstandard"
#r "Facades/netstandard" // https://github.com/ionide/ionide-vscode-fsharp/issues/839#issuecomment-396296095
#endif

open Fake.Core
open Fake.DotNet


module Tools = 
    let platformTool tool winTool =
        let tool = if Environment.isUnix then tool else winTool
        match ProcessUtils.tryFindFileOnPath tool with
        | Some t -> t
        | _ ->
            let errorMsg =
                tool + " was not found in path. " +
                "Please install it and make sure it's available from your path. " +
                "See https://safe-stack.github.io/docs/quickstart/#install-pre-requisites for more info"
            failwith errorMsg

    let nodeTool = platformTool "node" "node.exe"
    let yarnTool = platformTool "yarn" "yarn.cmd"
    let npmTool  = platformTool "npm" "npm.cmd"

    let runTool cmd args workingDir =
    
        let arguments = args |> String.split ' ' |> Arguments.OfArgs
        Command.RawCommand (cmd, arguments)
        |> CreateProcess.fromCommand
        |> CreateProcess.withWorkingDirectory workingDir
        |> CreateProcess.ensureExitCode
        |> Proc.run
        |> ignore

    let runDotNet cmd workingDir =
        let result =
            DotNet.exec (DotNet.Options.withWorkingDirectory workingDir) cmd ""
        if result.ExitCode <> 0 then failwithf "'dotnet %s' failed in %s" cmd workingDir

    let openBrowser url =
        //https://github.com/dotnet/corefx/issues/10361
        Command.ShellCommand url
        |> CreateProcess.fromCommand
        |> CreateProcess.ensureExitCodeWithMessage "opening browser failed"
        |> Proc.run
        |> ignore

    let dockerUser = "kmd"
    let dockerFullName (dockerImageName : string) = sprintf "%s/%s" dockerUser (dockerImageName.ToLower())
    let dockerTargetWithArgs (dockerImageName : string) dockerfile args _ = 
        let dockerImageName = dockerImageName.ToLower()
        let qualifiedDockerName = dockerFullName dockerImageName
        let buildArgs = 
            let args = 
                System.String.Join(" ", [
                                            yield sprintf "-t %s" qualifiedDockerName
                                            if args |> Option.isSome then yield System.String.Join(" ",
                                                                           args.Value
                                                                           |> List.map(fun (key,value) ->
                                                                               sprintf "--build-arg %s=%s" key value
                                                                           ))
                                            if dockerfile |> Option.isSome then yield "-f " + dockerfile.Value
                                        ])
                   
            sprintf "build %s ." args
        runTool "docker" buildArgs "."

        let tagArgs = sprintf "tag %s %s" qualifiedDockerName qualifiedDockerName
        runTool "docker" tagArgs "."

    let dockerTarget dockerImageName = 
        dockerTargetWithArgs dockerImageName None None

    