#r "paket: groupref build //"
#load "../Shared/build-template.fsx"
open ``Build-template``
open BuildBase.Tools

open Fake.Core


let imageName = "collector"
Target.create "Docker" (dockerTarget imageName)
Target.create "Push" (fun _ ->
    let tagArgs = sprintf "tag kmd/%s runefs/%s" imageName imageName
    let pushArgs = sprintf "push runefs/%s" imageName
    runTool "docker" tagArgs "."
    runTool "docker" pushArgs "."
)

Target.create "Compile" ignore //required by main build script but nothing to build here
Target.runOrDefaultWithArguments "Docker"