// Learn more about F# at http://fsharp.org
open Config
open Giraffe
open Saturn
open FSharp.Data
open FSharp.Data.HttpRequestHeaders

[<Literal>]
let dataExampleJson = """test files/data-example.json"""
[<Literal>]
let datasetExampleJson ="""test files/dataset-example.json"""

let isOkayStatusCode sc =
    sc >= 200 && sc < 400

type private AzureDevOpsData = FSharp.Data.JsonProvider<dataExampleJson>
type private AzureDevOpsDataRoot = FSharp.Data.JsonProvider<dataExampleJson>.Root
type private AzureDevOpsDataset = FSharp.Data.JsonProvider<datasetExampleJson>

//Sends HTTP-request to the databse for the dataset with the given name "name"
let collectDataFromDatabase name =
    try
        let response =
            Http.Request
                ( "http://azuredevopsdb:5984/azuredevops/_find",
                headers = [ Accept HttpContentTypes.Json; ContentType HttpContentTypes.Json],
                body = TextRequest
                    (sprintf " {
                        \"selector\": {
                            \"name\": \"%s\"
                        },
                        \"fields\": [\"value\"]
                    } " name),
                httpMethod = "POST",
                silentHttpErrors = true)
        let stringResponse = response.Body.ToString()
        //The toString method adds additional charachters that has to be removed to be able to parse
        let length = stringResponse.Length-2
        let rawData = stringResponse.[8..length] |> AzureDevOpsData.Parse
        if rawData.Error = "" then () else failwith rawData.Error
        (rawData, response.StatusCode)
    with e ->
        printf "azuredevopscollecter failed with: %A" e
        (AzureDevOpsData.GetSample (), 500)

let makeDB db =
    try Http.Request ( sprintf "http://azuredevopsdb:5984/%s" db, httpMethod = "PUT", silentHttpErrors = true) |> ignore
    with e -> printf "Failed to create database: %A" e

//Sends HTTP-request to the databse to get all the currently stored datasets
let getDatasets () =
    try
        let response =
            Http.Request
                ( "http://azuredevopsdb:5984/azuredevops/_find",
                headers = [ Accept HttpContentTypes.Json; ContentType HttpContentTypes.Json],
                body = TextRequest
                    (sprintf " {
                        \"selector\": {
                            \"_id\": { \"$gt\": null }
                        },
                        \"fields\": [\"name\"]
                    } "),
                httpMethod = "POST",
                silentHttpErrors = true)
        let stringResponse = response.Body.ToString()
        let length = stringResponse.Length-2
        let jsonData = stringResponse.[8..length]
        (*let jsonData = if jsonData = """{
                            "error": "not_found",
                            "reason": "Database does not exist."
                        }""" then jsonData else failwith "Database does not exist."*)
        let rawData = jsonData |> AzureDevOpsDataset.Parse
        if rawData.Error = "" then () else failwith rawData.Error
        let datasets = rawData.Docs |> Array.fold(fun lst v -> v.Name::lst) [] |> Array.ofList
        datasets
    with e ->
        printf "azuredevopscollecter failed with: %A" e
        ["_global_changes"; "_replicator"; "_users"; "azuredevops"]
        |> List.iter (fun db -> db |> makeDB)
        [||]



let addValOrDefault key objectToAdd defaultValue (map: Map<_,_>) =
    match objectToAdd with
    | null -> map.Add(key, (defaultValue |> box) ::map.[key])
    | v -> map.Add(key, (objectToAdd |> box)::map.[key])

let formatDateTimeOffset (date : System.DateTimeOffset) = "dd MMM yyyy" |> date.Date.ToString
let formatOptionDateTimeOffset (x : System.DateTimeOffset Option) =
    match x with
    | Some x -> formatDateTimeOffset x
    | None   -> " " //(System.DateTimeOffset.MinValue |> formatDateTimeOffset)

let formatData (rawData : AzureDevOpsDataRoot)  =
    let valueMap =
        ["WorkItemType"
         "Sprint"
         "SprintStartDate"
         "SprintEndDate"
         "StoryPoints"
         "WorkItemId"
         "State"
         "LeadTimeDays"
         "CycleTimeDays"
         "Project"]
        |> List.map (fun c -> (c, []))
        |> Map.ofList
    let data = rawData.Docs |> Array.fold(fun _ v -> v.Value) null
    let values =
         data
                |> Array.fold(fun (map: Map<_,_>) v ->
                   map
                   |> addValOrDefault "WorkItemType"    v.WorkItemType                                           " "
                   |> addValOrDefault "WorkItemId"      (box v.WorkItemId)                                       " "
                   |> addValOrDefault "Sprint"          v.Iteration.IterationName                                " "
                   |> addValOrDefault "SprintStartDate" (v.Iteration.StartDate  |> formatOptionDateTimeOffset)   " "
                   |> addValOrDefault "SprintEndDate"   (v.Iteration.EndDate |> formatOptionDateTimeOffset)      " "
                   |> addValOrDefault "StoryPoints"     (box v.StoryPoints)                                      " "
                   |> addValOrDefault "State"           (box v.State)                                            " "
                   |> addValOrDefault "LeadTimeDays"    (box v.LeadTimeDays)                                     " "
                   |> addValOrDefault "CycleTimeDays"   (box v.CycleTimeDays)                                    " "
                   |> addValOrDefault "CycleTimeDays"   (box v.CycleTimeDays)                                    " "
                   |> addValOrDefault "Project"         (box v.ProjectSk)                                        " "
                ) valueMap
    let matrix =
        let valueArray = values |> Map.toArray
        {
            ColumnNames = valueArray
                          |> Array.map (fun elements -> fst elements)
            Values = valueArray
                     |> Array.map (fun elements -> (snd elements) |> List.toArray)
        }
    matrix

let collectData name  =
    name |> collectDataFromDatabase |> fst
    |> formatData

let handleDataRequest name next ctx =
    let data = collectData name
    json data next ctx

let handleDatasetRequest next ctx =
    let data =  getDatasets ()
    json data next ctx


let route = router {
    getf "/data/%s" handleDataRequest
    get "/datasets" handleDatasetRequest
}

[<EntryPoint>]
let main argv =
    let app = Config.createApp route
    run app
    0