module Config

open System.IO
open System.Threading.Tasks
open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.DependencyInjection
open FSharp.Control.Tasks.V2
open Giraffe
open Saturn

type Datasets =
    {
        Datasets : string []
    }

type Matrix =
    {
        ColumnNames : string []
        Values : obj [] []
    }

let tryGetEnv = System.Environment.GetEnvironmentVariable >> function null | "" -> None | x -> Some x
let port = "SERVER_PORT" |> tryGetEnv |> Option.map uint16 |> Option.defaultValue 8085us

let configureSerialization (services:IServiceCollection) =
    services.AddSingleton<Giraffe.Serialization.Json.IJsonSerializer>(Thoth.Json.Giraffe.ThothSerializer())

let createApp route = application {
    url ("http://0.0.0.0:" + port.ToString() + "/")
    use_router route
    memory_cache
    service_config configureSerialization
    use_gzip
}
