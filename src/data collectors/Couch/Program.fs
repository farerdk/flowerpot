// Learn more about F# at http://fsharp.org
open Config
open Giraffe
open Saturn
open System.IO
open System

[<Literal>]
let FilesFolder = "/Server/Files"

[<Literal>]
let ExampleJson = """{
  "rows": [
    {
      "key": null,
      "value": {
        "ColumnNames": [
          "team",
          "sprint",
          "effort",
          "velocity"
        ],
        "Values": [
          [
            "team 1",
            "team 1",
            "team 2",
            "team 2"
          ],
          [
            1.4,
            2.3,
            1.05,
            2.0234
          ],
          [
            58.1,
            80,
            70,
            "75"
          ],
          [
            47,
            45,
            45,
            "35"
          ]
        ]
      }
    }
  ]
}"""

type private Data = FSharp.Data.JsonProvider<ExampleJson>
type private Dataset = FSharp.Data.JsonProvider<"""[
                          "_global_changes",
                          "_replicator",
                          "_users",
                          "azuredevops",
                          "azuredevopsdb",
                          "game"
                        ]""">

let collectData name =
    let fileName = sprintf "%s/%s.csv" FilesFolder name
    let matrix =
        if fileName |> System.IO.File.Exists then
            let lines =
                fileName
                |> System.IO.File.ReadAllLines
                |> Array.map (fun a -> a.Split(','))
            {
                ColumnNames = lines |> Array.head
                Values =
                    lines
                    |> Array.tail
                    |> Array.transpose
                    |> Array.map(fun row ->
                        let f =
                           let head = row |> Array.head
                           match head |> System.Double.TryParse with
                           true, _ ->
                               System.Double.Parse >> box
                           | _ ->
                               match head |> System.DateTimeOffset.TryParse with
                               true,_ ->
                                   System.DateTimeOffset.Parse >> box
                               | _ -> box
                        row |> Array.map f
                    )
            }
        else
            let data = name |> sprintf  "http://couch_collector_db:5984/%s/_design/default/_view/matrix" |> Data.Load
            data.Rows
            |> Array.fold(fun res row ->
              let record = row.Value
              { res with
                  ColumnNames =
                      record.ColumnNames
                      |> Array.append res.ColumnNames
                  Values =
                      record.Values
                      |> Array.map(fun ns ->
                         match ns.Numbers with
                         [||] ->
                             ns.Strings |> Array.map box
                         | _ ->
                             ns.Numbers |> Array.map box
                      ) |> Array.append res.Values
              }
            ) {
                ColumnNames = [||]
                Values = [||]
              }
    matrix


//Runs through the testfiles, to retrieve the names of the datasets
let getDatasets () =
     (try
         Dataset.Load "http://couch_collector_db:5984/_all_dbs"
         |> Array.filter(fun name -> name.StartsWith "_" |> not)
      with e ->
        e.ToString() |> eprintf "%s"
        [||])
     |> Array.append(
        let files =
             System.IO.Directory.GetFiles(FilesFolder,"*.csv")
             |> Array.map(System.IO.Path.GetFileNameWithoutExtension)
        printf "Found data files: %A" files
        files
     )

let handleDataRequest name next ctx =
    let data = collectData name
    json data next ctx

let handleDatasetRequest next ctx =
    let data = {Datasets = getDatasets ()}
    json data next ctx

let route = router {
    getf "/data/%s" handleDataRequest
    get "/datasets" handleDatasetRequest
}

[<EntryPoint>]
let main argv =
    let app = Config.createApp route
    run app
    0