// Learn more about F# at http://fsharp.org
open Config
open Giraffe
open Saturn
open LibGit2Sharp

[<Literal>]
let private RFC2822Format = "ddd dd MMM HH:mm:ss yyyy K";

let private root = "/etc/repos"
let collectData name  =
    let repoPath = System.IO.Path.Combine(root, name)

    use repo = new Repository(repoPath)


    let matrix =
        {
            ColumnNames =
               [|
                   "Time Between Commits"
                   "Date"
                   "Author Name"
                   "Author E-mail"
                   "Commit Message"
               |]
            Values =
                let columns =
                    repo.Commits
                    |> Seq.sortByDescending(fun commit -> commit.Author.When)
                    |> Seq.take(250)
                    |> Seq.map(fun commit ->
                        let dtOffset : System.DateTimeOffset = commit.Author.When
                        let authorName = commit.Author.Name
                        let authorEmail = commit.Author.Email
                        let message = commit.Message
                        [
                            dtOffset :> obj
                            authorName :> obj
                            authorEmail :> obj
                            message :> obj
                        ]
                    ) |> List.ofSeq
                    |> List.transpose
                let timeBetweenCommits =
                    let commitDates =
                        columns.[0]
                        |> List.map (fun d -> d :?> System.DateTimeOffset)
                        |> List.sortDescending
                    commitDates.Tail
                    |> List.fold(fun (prev, lst) dt ->
                        (dt, ((prev - dt).TotalMinutes |> box)::lst)
                    ) (commitDates.Head, [0])
                    |> snd

                timeBetweenCommits::columns
                |> List.map Array.ofList
                |> Array.ofList


        }
    matrix

//Runs through the testfiles, to retrieve the names of the datasets
let getDatasets () =
    System.IO.Directory.EnumerateDirectories root
    |> Seq.map(fun n -> n.Split([|"/";"\\"|], System.StringSplitOptions.RemoveEmptyEntries) |> Array.last)
    |> Array.ofSeq

let handleDataRequest name next ctx =
    let data = collectData name
    json data next ctx

let handleDatasetRequest next ctx =
    let data = {Datasets = getDatasets ()}
    json data next ctx


let route = router {
    getf "/data/%s" handleDataRequest
    get "/datasets" handleDatasetRequest
}

[<EntryPoint>]
let main argv =
    let app = Config.createApp route
    run app
    0