#r "paket: groupref build //"
#load "../Shared/build-template.fsx"
open ``Build-template``
open BuildBase.Tools

open Fake.Core

let imageName = "git_data_collector"
Target.create "Docker" (dockerTarget imageName)
Target.create "Push" (fun _ ->
    let tagArgs = sprintf "tag kmd/%s runefs/%s" imageName imageName
    let pushArgs = sprintf "push runefs/%s" imageName
    runTool "docker" tagArgs "."
    runTool "docker" pushArgs "."
)



"." |> Fake.IO.Path.getFullName |> createBuildAndRunTargets

Target.create "Compile" ignore
open Fake.Core.TargetOperators

"Clean"
    ==> "Build"
    ==> "Bundle"
    ==> "Compile"

"Clean"
    ==> "Run"
Target.runOrDefaultWithArguments "Docker"