# Date Collectors

Any build.fsx file will be pick up and it's expected to have a docker target.
When booting up the environment (fake build --target up in the root of the project). All data collectors will be started in their own docker container.
They are accessible through a reverse proxy http://localhost:8088/{collectorname}/ whenre the name is the name of the folder the build.fsx script for the collector is found