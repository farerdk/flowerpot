#r "paket: groupref build //"
#load "../../buildBase.fsx"

#if !FAKE
#r "netstandard"
#r "Facades/netstandard" // https://github.com/ionide/ionide-vscode-fsharp/issues/839#issuecomment-396296095
#endif

open Fake.Core
open Fake.IO
open BuildBase.Tools

let deployDir = Path.getFullName "./deploy"

Target.create "Clean" (fun _ ->
    [ deployDir ]
    |> Shell.cleanDirs
)

let createBuildAndRunTargets serverPath =
    Target.create "Build" (fun _ ->
        runDotNet "publish" serverPath
    )

    Target.create "Run" (fun _ ->
        let server = async {
            runDotNet "watch run" serverPath
        }

        let tasks =
            [ server ]

        tasks
        |> Async.Parallel
        |> Async.RunSynchronously
        |> ignore
    )

    Target.create "Bundle" (fun _ ->
        let serverDir = Path.combine deployDir "Server"

        let publishArgs = sprintf "publish -c Release -o \"%s\"" serverDir
        runDotNet publishArgs serverPath
    )