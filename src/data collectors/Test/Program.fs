// Learn more about F# at http://fsharp.org
open Config
open Giraffe
open Saturn
open System.IO
open System

[<Literal>]
let exampleJson = """test files/velocity-data.json"""
[<Literal>]
let path = """test files/"""

type private TestData = FSharp.Data.JsonProvider<exampleJson>

let collectData name  =
    let filepath = sprintf "test files/%s-data.json" name
    let data = TestData.Load filepath
    let matrix =
        {
            ColumnNames = data.ColumnNames
            Values =
                data.Values
                |> Array.map(fun v ->
                    match v.Numbers with
                    [||] ->
                        match v.Strings with
                        [||] ->
                            v.DateTimes
                            |> Seq.cast<obj>
                            |> Array.ofSeq
                        | strings ->
                            strings
                            |> Seq.cast<obj>
                            |> Array.ofSeq
                    | numbers ->
                        numbers
                        |> Seq.cast<obj>
                        |> Array.ofSeq
                )
        }

    printf "Data collected: %A" matrix
    matrix

//Runs through the testfiles, to retrieve the names of the datasets
let getDatasets () =
    System.IO.Directory.GetFiles(path, "*.json")
    |> Array.map (fun file -> Path.GetFileName(file).Split([|"-"|], StringSplitOptions.None).[0])

let handleDataRequest name next ctx =
    let data = collectData name
    json data next ctx

let handleDatasetRequest next ctx =
    let data = {Datasets = getDatasets ()}
    json data next ctx


let route = router {
    getf "/data/%s" handleDataRequest
    get "/datasets" handleDatasetRequest
}

[<EntryPoint>]
let main argv =
    let app = Config.createApp route
    run app
    0