module Fetch

type DataResult<'a> =
        Success of 'a
        | Unauthorized of endpoint : string
        | BadRequest of endpoint : string
        with static member bind f dr : DataResult<'b> =
                match dr with
                | Success s -> f s
                | Unauthorized e -> Unauthorized e
                | BadRequest e -> BadRequest e

type FetchWorkflow() =
        member this.Bind(m, f ) =
            DataResult<_>.bind f m

        member this.Return(x) = Success x

        member this.ReturnFrom(x) = x

        member this.Yield(x) = Success x

        member this.YieldFrom(x) = x

        member this.Zero() = this.Return ()

        member this.Delay(f) = f

        member this.Run(f) = f()

        member this.While(guard, body) =
            if not (guard())
            then this.Zero()
            else this.Bind( body(), fun () ->
                this.While(guard, body))

        member this.TryWith(body, handler) =
            try this.ReturnFrom(body())
            with e -> handler e

        member this.TryFinally(body, compensation) =
            try this.ReturnFrom(body())
            finally compensation()

        member this.Using(disposable:#System.IDisposable, body) =
            let body' = fun () -> body disposable
            this.TryFinally(body', fun () ->
                match disposable with
                    | null -> ()
                    | disp -> disp.Dispose())

        member this.For(sequence:seq<_>, body) =
            this.Using(sequence.GetEnumerator(),fun enum ->
                this.While(enum.MoveNext,
                    this.Delay(fun () -> body enum.Current)))

    let fetch = FetchWorkflow()