open Saturn
open Shared.Hobbes
open Fable.Remoting.Server
open Fable.Remoting.Giraffe
open System
open System.IO

[<Literal>]
let exampleJson = """../../data collectors/Test/test files/velocity-data.json"""

exception CollectorFailedException of string

let port = 8085
type DataMatrix = FSharp.Data.JsonProvider<exampleJson>
type Dataset = FSharp.Data.JsonProvider<"""{ "Datasets": ["name1", "name2", "name3"] }""">
let api = {
    data =
        fun collectorId dataset transformationList ->
        
              let collectorUrl = sprintf "http://collector/%s/data/%s" (collectorId.ToLower()) (dataset.ToLower())              
              //fetch data from collector
              let result =
                  let table : Hobbes.Parsing.Compile.Table = 
                      try 
                          let data = DataMatrix.Load collectorUrl
                          
                          let columnNames = data.ColumnNames
                          let arraysToISeriesInt array = array |> Array.map box |> Seq.ofArray
                          let values = 
                              data.Values
                              |> Array.map(fun value ->
                                  //Only one of them will actually have any objects
                                  match value.Numbers with
                                  [||] ->
                                     match value.Strings with
                                     [||] -> 
                                         value.DateTimes |> arraysToISeriesInt
                                     | strings ->
                                        strings
                                        |> Array.map(fun s ->
                                            match  s |> System.DateTimeOffset.TryParse with
                                            false,_ -> s :> obj
                                            | true,d -> d :> obj
                                        ) |> Seq.ofArray
                                  | numbers ->
                                      numbers |> arraysToISeriesInt
                              ) 
                          values
                          |> Seq.ofArray
                          |> Seq.zip columnNames
                      with e ->
                          e.ToString() |> eprintfn "Error: %s" 
                          Seq.empty
                  try 
                      if Seq.isEmpty table then raise (CollectorFailedException(sprintf "%s failed when retrieving %s" collectorId dataset))
                      //find the transformation and transform data here using the hobbes language
                      //open transformationFile
                      //System.Console.WriteLine(transformationName)
                      let transformedTable = 
                          if List.isEmpty transformationList then (Hobbes.Parsing.Compile.expressions ["only 1=2"]) table
                          else 
                              let filePathList = List.map (fun name -> sprintf "transformations/%s.ht" name) transformationList
                              let transformations = 
                                  let transformations = 
                                      List.map System.IO.File.ReadAllLines filePathList
                                      |> List.map  (Array.map (fun s -> s.Trim()))
                                      |> List.map (Array.filter(fun s -> System.String.IsNullOrWhiteSpace(s) |> not))
                                  transformations
                                  |> List.map (Hobbes.Parsing.Compile.expressions)
                              List.fold (fun table transformation -> transformation table ) table transformations
                      printfn "%A" transformedTable
                      transformedTable
                      |> Seq.map (fun (name,values) ->
                          //there's a bug in the remoting framework when trying to deserialize object collections
                          //so everything is serialized to a string and an actual type is provided for deserialisation
                          let values = 
                              values 
                              |> List.ofSeq
                          {
                               Name = name
                               Values = 
                                  values
                                  |> List.map string
                               Type = 
                                   match values
                                         |> List.tryFind(fun v -> v |> isNull |> not) with
                                   None -> DataType.String
                                   | Some v ->
                                       match v with
                                       :? int
                                       | :? float
                                       | :? decimal -> DataType.Number
                                       | :? DateTimeOffset -> DataType.DateTimeOffset
                                       | :? string -> DataType.String
                                       | _ -> failwithf "unknown type %A" v
                          }
                      ) |> Array.ofSeq
                    with e ->
                        e.ToString() |> eprintfn "Error: %s" 
                        [||]

              async { return result };
    //Retrieves the dataset from each collect and returns them as a map, that maps Collector names (string) to datasets (string[])
    dataset = 
        fun collectorList ->
                async {
                    return  
                        collectorList
                        |> List.fold (fun acc collector ->
                            try
                                let datasetUrl = sprintf "http://collector/%s/datasets" (collector.ToLower())
                                let rawdata = Dataset.Load datasetUrl
                                let datasets = rawdata.Datasets
                                Map.add collector datasets acc
                            with e ->
                                e.ToString() |> eprintfn "Error: %s" 
                                acc
                        ) Map.empty
                }
            
    transformations =
        fun () -> 
           async {
               let res = 
                    Directory.EnumerateFiles("transformations", "*.ht")
                    |> List.ofSeq
                    |> List.map System.IO.Path.GetFileNameWithoutExtension
               return
                   if res |> List.isEmpty then
                       System.IO.File.WriteAllText("transformations/empty.ht","")
                       ["empty"]
                   else res
            }
            
    saveFile =
        fun transformationName transformationBody -> 
            printfn "Savefile %A and %A" transformationName transformationBody
            async{
                let filePath = System.IO.Path.Combine ("transformations" , transformationName+ ".ht")
                System.IO.File.WriteAllText (filePath, transformationBody)
            }
    readFile =
        fun transformationName -> 
            printfn "Read file: %A" transformationName
            async{
                let filePath = System.IO.Path.Combine ("transformations" , transformationName+ ".ht")
                return System.IO.File.ReadAllText (filePath)
            }
    deleteFile =
        fun transformationName -> 
            printfn "Delete file: %A" transformationName
            async{
                let filePath = System.IO.Path.Combine ("transformations" , transformationName+ ".ht")
                return System.IO.File.Delete (filePath)
            }
    validateTransformation = 
        fun transformationBody ->
            async {
                try 
                    let transformationBody = transformationBody.Split('\n')
                                             |> Array.map (fun s -> s.Trim())
                                             |> Array.filter(fun x -> String.IsNullOrWhiteSpace x |> not)
                    printf "%A" transformationBody
                    Hobbes.Parsing.Compile.expressions transformationBody Seq.empty
                    |> ignore
                    return None 
                with e -> return e.Message |> Some
            }
}


let webApp =
    Remoting.createApi()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.fromValue api
    |> Remoting.buildHttpHandler

let app = application {
    url (sprintf "http://0.0.0.0:%d/" port)
    use_router webApp
    memory_cache
    use_gzip
}

let _ = 
    match api.transformations() |> Async.RunSynchronously with
    [] -> 
        System.IO.File.WriteAllLines(System.IO.Path.Combine("transformations", "all.ht"), [|"only 1=1"|])
    | _ -> ()
   
run app
