namespace Shared.Hobbes

module Route =
    /// Defines how routes are generated on server and mapped from client
    let builder typeName methodName =
        sprintf "/api/%s/%s" typeName methodName
type DataType = 
     Number
     | String
     | DateTimeOffset

    


type DataColumn = 
    {
        Name : string
        Values : string list
        Type : DataType
    }

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
module DataColumn =
    let isNumeric (dc : DataColumn) =
        dc.Type = Number
        
type IDataService =
    {
        data : string -> string -> string list -> Async<DataColumn []>
        dataset : string list -> Async<Map<string, string[]>>
        transformations : unit -> Async<string list>
        saveFile : string -> string -> Async<unit>
        readFile : string -> Async<string>
        deleteFile : string -> Async<unit>
        validateTransformation: string -> Async<string Option>
    }