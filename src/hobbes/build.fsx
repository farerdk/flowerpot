#r "paket: groupref build //"
#load "../buildBase.fsx"

#if !FAKE
#r "netstandard"
#r "Facades/netstandard" // https://github.com/ionide/ionide-vscode-fsharp/issues/839#issuecomment-396296095
#endif

open Fake.Core
open Fake.IO
open BuildBase.Tools

let serverPath = Path.getFullName "./Server"

let clientDeployPath = Path.combine serverPath "deploy"
let deployDir = Path.getFullName "./deploy"

Target.create "Clean" (fun _ ->
    [ deployDir
      clientDeployPath ]
    |> Shell.cleanDirs
)

Target.create "Build" (fun _ ->
    runDotNet "build" serverPath
)

Target.create "Run" (fun _ ->
    let server = async {
        runDotNet "watch run" serverPath
    }

    let tasks =
        [ server ]

    tasks
    |> Async.Parallel
    |> Async.RunSynchronously
    |> ignore
)

Target.create "Bundle" (fun _ ->
    let serverDir = Path.combine deployDir "Server"

    let publishArgs = sprintf "publish -c Release -o \"%s\"" serverDir
    runDotNet publishArgs serverPath
)

let imageName = "hobbes"
Target.create "Docker" (dockerTarget imageName)   
Target.create "Push" (fun _ ->         
    let tagArgs = sprintf "tag kmd/%s runefs/%s" imageName imageName
    let pushArgs = sprintf "push runefs/%s" imageName
    runTool "docker" tagArgs "."
    runTool "docker" pushArgs "."
)


open Fake.Core.TargetOperators
Target.create "Compile" ignore
"Clean" 
    ==> "Build" 
    ==> "Bundle" 
    ==> "Compile"

"Bundle" ==> "Docker"
"Bundle" ==> "Run"
Target.runOrDefaultWithArguments "Docker"