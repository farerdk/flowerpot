namespace Parsing

module Arithmetic =
    open FParsec.Primitives
    open FParsec.CharParsers
    open Parsing
    open Parsing.Basic


    let private opAddition : Parser<_>   = skipString "+"
    let private opSubtraction : Parser<_>   = skipString "-"
    let private opMultiplication : Parser<_>   = skipString "*"
    let private opDivision : Parser<_>   = skipString "/"
    let private opArrow : Parser<_>   = skipString "=>"

    let private expression operator f = 
        pipe3 (identifier .>> operator) identifier (opArrow >>. identifier) (fun lhs rhs newColumnName ->
             f(lhs,rhs,newColumnName)
        )
    let addition = expression opAddition AST.Addition
    let subtraction = expression opSubtraction AST.Subtraction
    let multiplication = expression opMultiplication AST.Multiplication
    let division = expression opDivision AST.Division

    let parse =  
       addition
       <|> subtraction
       <|> multiplication
       <|> division
       >>= (AST.Arithmetic >> preturn)