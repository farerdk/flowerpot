module Tests

open System
open Xunit
open Shared.Hobbes
open Visualiser

[<Fact>]
let VisualiserMeansTest() = 
    let result =
        [(1.5, 2.5); (2.5, 1.5); (10.5, 20.5)]
        |> meanOfCoords
    let expected = ((1.5+2.5+10.5)/3.0, (2.5+1.5+20.5)/3.0)
    Assert.Equal(expected, result)

[<Fact>]
let VisualiserBestLineFromCords () =
    //https://www.wolframalpha.com/input/?i=linear+fit+%7B7.0,+2.0%7D,+%7B13.6,+5.3%7D,+%7B44.0+20.5%7D
    let (a, b, rSquared) =
        [(7.0, 2.0); (13.6, 5.3); (44.0, 20.5)]
        |> bestLineFromCords
    let result = (int (a *10.0), int (b *10.0), int (rSquared *10.0))
    let expected = (5, -15, 1 )
    Assert.Equal(expected, result)

[<Fact>]
let VisualiserBestLineFromNoCords () =
    let result = [] |> Visualiser.bestLineFromCords
    let expected = (0.0, 0.0)
    Assert.Equal(expected.ToString(), result.ToString())

[<Fact>]
let VisualiserBestLines() =
    let rowDrawAble = [|
        {Name = "y1"; Values = ["2.0"; "5.3"; "20.5"]; Type = Number}
        |]
    let xAxisDc = {Name = "x"; Values = ["7.0"; "13.6"; "44.0"]; Type = Number}
    let result = Visualiser.bestLines rowDrawAble xAxisDc
                 
    let result = result |> Array.map(fun (s, (a, b, rSquared)) -> (s, (int (a * 10.0), int (b * 10.0), int (rSquared *10.0))) )

    let expected =[("y1", (5, -15, 1))]
    Assert.Equal(expected, result)


[<Fact>]
let VisualiserCombineValuesOfDatacolumnStacks() =
    let rowDrawAble = [|
        {Name = "y1"; Values = ["2.0"; "5.3"; "20.5"]; Type = Number}
        {Name = "y2"; Values = ["1.0"; "5.4"; "3.5"]; Type = Number}
        {Name = "y3"; Values = ["12.0"; "50.4"; "33.5"]; Type = Number}
        |]
    
    let result = CombineValuesOfDatacolumnStacks rowDrawAble ([("y1", 1);("y2", 1);("y3", 2) ] |> Map.ofList)

    let expected =[|
            {Name = "y1"; Values = ["3.0"; "10.7"; "24.0"]; Type = Number}
            {Name = "y3"; Values =  ["12.0"; "50.4"; "33.5"]; Type = Number}
        |]

    Assert.Equal(expected.ToString (), result.ToString ())