﻿#r "../metrics/packages/Deedle/lib/net40/Deedle.dll"
#r "../metrics-core/metrics-core/bin/Debug/netcoreapp2.1/metrics-core.dll"


open Deedle
open FParsec.CharParsers
open FParsec
open Deedle.Internal


let testFiles = 
    [
        "slice columns"
        "groupBy"
    ] |> List.map(fun f ->
        f,sprintf "../../../test files/%s.tz" f
    ) |> Map.ofList

let numericColumnNames = 
    [
        "Sprint"
        "Story Points"
        "Work Item Id"
        "Lead Time Days"
        "Cycle Time Days"
    ]
    
let dateColumnNames = 
    [
        "Sprint Start Date"
        "Sprint End Date"
    ]

let textColumns = 
    [
        "Team"
        "State"
        "Work Item Type"
    ]
let cellValue i j = 
    j * (i + (j % (i + 1)) * 1)

let attachColumns columnNames producer frame = 
    columnNames
    |> List.indexed
    |> List.fold(fun frame (i,colName) ->
        frame
        |> Frame.addCol
            colName
            (
                [for j in 0..10 ->
                   cellValue i j
                   |> producer
                 ] |> List.indexed
                 |> series
            )
    ) frame

let frame = 
    Frame([],[])
    |> attachColumns numericColumnNames id
    |> attachColumns textColumns (fun n ->
        char(int('a') + n) |> string
    ) |> attachColumns dateColumnNames (fun n ->
        n |> float |> System.DateTime(2019,1,1).AddDays
    )

let compileFileAndExecute file = 
    let expressions = 
         testFiles
         |> Map.find file
         |> Parsing.Core.parseExpressionFile
         |> Compile.expressions
    frame |> expressions
let assertColumns expected actual = 
    let distinct seq1 seq2 =
        seq1
        |> Seq.filter(fun e ->
           seq2 
           |> Seq.tryFind (fun a -> a = e) 
           |> Option.isNone
        ) |> Seq.isEmpty

    let haveAllExpected = 
        distinct expected actual

    let noExtra = 
        distinct actual expected

    haveAllExpected && noExtra

let groupBy() = 
    let resultFrame : Frame<_,_> = 
        "groupBy"
        |> compileFileAndExecute
    let columnNames = 
        resultFrame.GetAllColumns()
        |> Seq.map(fun kv -> kv.Key)
        
    let expectedColumns = 
        [
            "Sprint"
            "Team"
            "Story Points"
        ]
    assert(assertColumns expectedColumns columnNames)
    assert(resultFrame.RowCount = 2)
     
let sliceColumns() = 
    let resultFrame : Frame<_,_> = 
       "slice columns"
       |> compileFileAndExecute

    
    let columnNames = 
        resultFrame.GetAllColumns()
        |> Seq.map(fun kv -> kv.Key)
        |> Set.ofSeq
    let expectedColumns = 
        [
            "Queue Time"
            "Cycle Time Days"
        ]
    assert(columnNames |> Seq.length = expectedColumns.Length)
    assert(expectedColumns
           |> List.fold(fun res name ->
               columnNames |> Set.contains name && res
           ) true)