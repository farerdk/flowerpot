﻿// Learn more about F# at http://fsharp.org

open Deedle
open FParsec.CharParsers
open FParsec
open Deedle.Internal
open Deedle.FrameBuilder

let inline private equal expected actual msg = 
      if actual = expected |> not then failwithf "%A <> %A %s" actual expected msg


let inline private ``moving mean`` serie = 
    let rec inner serie (l1,l2) result =
        match serie with
        [] -> result |> List.rev
        | head::tail ->  
            match l1, l2 with
            None, None -> inner tail (Some head, None) result
            | _, None | None, _ ->  inner tail (Some head, l1) result
            | Some l1, Some l2 ->
                inner tail (Some head, Some l1) ((float(head + l1 + l2) / 3.)::result)
    inner (serie |> List.map (unbox >> float)) (None,None) []
                
let inline private average serie = 
    serie
    |> List.averageBy (unbox >> float)

[<Literal>]
let root = "../../../"

let testFiles = 
    [
        "slice columns"
        "groupBy"
        "mean"
        "moving mean"
    ] |> List.map(fun f ->
        f,sprintf "%stest files/%s.tz" root f
    ) |> Map.ofList

let numericColumnNames = 
    [
        "Sprint"
        "Story Points"
        "Work Item Id"
        "Lead Time Days"
        "Cycle Time Days"
    ]
    
let dateColumnNames = 
    [
        "Sprint Start Date"
        "Sprint End Date"
    ]

let textColumns = 
    [
        "Team"
        "State"
        "Work Item Type"
    ]

let cellValue i j = 
    (i + (j % (i + 1)))

let attachColumns columnNames (producer : int -> obj) columns  = 
    let colCount = columns |> List.length
    columnNames
    |> List.indexed
    |> List.map(fun (i,colName) ->
        colName,
             [for j in 0..10 ->
                   cellValue (i + colCount) j
                   |> producer
                 ]
    )
    |> List.append columns
    

let data = 
    []
    |> attachColumns numericColumnNames box
    |> attachColumns textColumns (fun n ->
        char(int('a') + n) |> string |> box
    ) |> attachColumns dateColumnNames (fun n ->
        n |> float |> System.DateTime(2019,1,1).AddDays |> box
    )

let dataSet = 
    data
    |> Map.ofList

let frame = 
    data
    |> List.fold(fun frame (colName, values) ->
        frame
        |> Frame.addCol
            colName
               (
                   values
                   |> List.indexed
                   |> series
               )
    ) (Frame([],[]))

let compileFileAndExecute file = 
    let expressions = 
         testFiles
         |> Map.find file
         |> Parsing.Core.parseExpressionFile
    let compiledExpressions = 
         expressions |> Compile.expressions
    frame 
    |> compiledExpressions

let assertColumns expected actual = 
    let distinct seq1 seq2 =
        seq1
        |> Seq.filter(fun e ->
           seq2 
           |> Seq.tryFind (fun a -> a = e) 
           |> Option.isNone
        ) |> Seq.isEmpty

    let haveAllExpected = 
        distinct expected actual

    let noExtra = 
        distinct actual expected

    if haveAllExpected && noExtra |> not then failwithf "incorrect columns detected. Expcted: %A Actual: %A" expected actual

let groupBy() = 
    let resultFrame : Frame<_,_> = 
        "groupBy"
        |> compileFileAndExecute
    let columnNames = 
        resultFrame.GetAllColumns()
        |> Seq.map(fun kv -> kv.Key)
        
    let expectedColumns = 
        [
            "Sprint"
            "Team"
            "Story Points"
        ]
    assertColumns expectedColumns columnNames
    equal resultFrame.RowCount 6 "Incorrect number of groups"
     
let sliceColumns() = 
    let resultFrame : Frame<_,_> = 
       "slice columns"
       |> compileFileAndExecute

    
    let columnNames = 
        resultFrame.GetAllColumns()
        |> Seq.map(fun kv -> kv.Key)
        |> Set.ofSeq

    let expectedColumns = 
        [
            "Sprint"
            "Team"
        ]

    assertColumns expectedColumns columnNames

let mean() = 
    let resultFrame : Frame<_,_> = 
       "mean"
       |> compileFileAndExecute
    
    equal 1 (resultFrame.RowCount) "Too many rows"
    let row = 
        resultFrame 
        |> Frame.rows
        |> Series.observations
        |> Seq.head
        |> snd

    let ``expexted average of Story Points`` =
       dataSet.["Story Points"]
       |> average
    let ``expexted average of Sprint no`` =
       dataSet.["Sprint"]
       |> average

    equal ``expexted average of Sprint no`` (row.GetAs<float> "Sprint") "Sprint calculated incorrectly"
    equal ``expexted average of Story Points`` (row.GetAs<float> "Story Points") "Story Points calculated incorrectly"

     
let movingMean() = 
    let resultFrame : Frame<_,_> = 
        "moving mean"
        |> compileFileAndExecute

    let ``expexted moving mean of Story Points`` =
        dataSet.["Story Points"]
        |> ``moving mean``
    
    let ``actual moving mean of Story Points`` = 
        resultFrame?``Story Points``
        |> Series.observations
        |> Seq.map snd
        |> List.ofSeq

    //the moving mean window is 3 so the first too columns have no value and are removed
    equal (dataSet.["Sprint"].Length - 2) (resultFrame.RowCount) "Too many rows"
    equal ``expexted moving mean of Story Points`` ``actual moving mean of Story Points`` "The moving mean differs"
         

[<EntryPoint>]
let main _ =
    while true do
        let tests = 
           [
               sliceColumns
               groupBy
               mean
               movingMean
           ]
        tests
        |> List.iter(fun t -> t())
        printfn "All tests passed"
        printfn "Press enter to rerun tests"
        System.Console.ReadLine() |> ignore
    0 // return an integer exit code
